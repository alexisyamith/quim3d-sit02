using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System;
using NSSeleccionMouse;
using System.IO;
using System.Linq;

#if !UNITY_WEBGL
using ValidacionMenu;
#endif

/// <summary>
/// Clase que controla algunos valores del simulador
/// </summary>
public class Simulador : MonoBehaviour
{
    /// <summary>
    /// El simulador tiene seguridad?
    /// </summary>
    public bool SeguridadSwitch;
    /// <summary>
    /// Se esta ejecutando el modo aula?
    /// </summary>
    public bool ModoAula; 

	public bool ModoLTI;

	public bool ModoMonoUsuario;

	public string mono_user_name = "";
	public string mono_user_institution = "";
	public string mono_user_email = "";



    /// <summary>
    /// /// Seguridad nueva datos
    /// /// </summary>
    private string[] cmd;
    private string correoMono;
    private string nombreMono;
    private string instituMono;

    /// <summary>
    /// IP del servidor para ejecutar procesos con el servidor, ejemplo : login
    /// </summary>
    public string url_aula = "http://190.147.220.194:3000";

	public string url_score_lti = "https://lti.servercloudlabs.com/lti_launcher/setScore";
	public string valid_lti_url = "lti.servercloudlabs.com";
    public string WebGLUrl;

    public string usuario;
	public string curso;
	public string id_curso;
	public string institucion;
	public string parametros_lti;

    /// <summary>
    /// Plataforma en la que se esta ejecutando
    /// </summary>
    public enum TipoAplicacion
    {
        WEB_LOCAL,
        WEB,
        ANDROID,
        IOS
    };    
    /// <summary>
    /// �Esta activado el boton reset?
    /// </summary>
    public static bool activeBtnReset = false;
    /// <summary>
    /// El usuario esta ejecutando actualmente un ejercicio?
    /// </summary>
    public static bool ejecutandoEjercicio = false;
    /// <summary>
    /// La simulacion en una estacion fue iniciada por el usuario?
    /// </summary>
    public static bool simulacionIniciada = false;
    /// <summary>
    /// Referencia a la clase que controla la hubicacion de la camara
    /// </summary>
    public Control control;
    /// <summary>
    /// Nombre/Id del curso
    /// </summary>
    //private string curso;
    /// <summary>
    /// Referencia a la clase que controla los valores de la interfaz
    /// </summary>
    public Pantalla pantalla;
    /// <summary>
    /// Punto a donde se mueve la camara para enfocar la estacion de simulacion de aminoacidos
    /// </summary>
    public Transform posFocoMesaAminoacido;
    /// <summary>
    /// Punto a donde se mueve la camara para enfocar la estacion de simulacion de destilacion
    /// </summary>
    public Transform posFocoMesaDestilacion;
    /// <summary>
    /// Punto a donde se mueve la camara para enfocar la estacion de simulacion de fermatacion
    /// </summary>
    public Transform posFocoMesaFermentacion;
    /// <summary>
    /// Referencia a la clase del simulador que actualmente esta activo
    /// </summary>
    public SimuladorGenerico simuladorActivo;
    /// <summary>
    /// Tipo de plataforma en la que se esta ejecutando el simulador
    /// </summary>
    public TipoAplicacion tipoAplicacion;
    /// <summary>
    /// Nombre del usuario
    /// </summary>
    //private string usuario;
    /// <summary>
    /// GameObject del boton para resetear la simulacion de la estacion de fermetacion
    /// </summary>
    public GameObject btnResetFermentacion;
    /// <summary>
    /// GameObject del boton para resetear la simulacion de la estacion de aminoacidos
    /// </summary>
    public GameObject btnResetAmino;
    /// <summary>
    /// GameObject del boton para resetear la simulacion de la estacion de destilacion
    /// </summary>
    public GameObject btnResetDestilacion;
    /// <summary>
    /// Array con todas las imagenes de los logos del simulador, esto para cambiarlos
    /// </summary>
    public Image[] logos;

    public Sprite logoV1;

    public Sprite logoV2;

    public string fileName = "";

    [SerializeField]
    private SeleccionMouse refSeleccionMouse;

    public void ActivarSeguridad()
    {
        SeguridadSwitch = true;
    }

    public void QuitarSeguridad()
    {
        SeguridadSwitch = false;
    }

    public void UsarLogo1()
    {
        foreach (Image logoActual in logos)
        {
            logoActual.sprite = logoV1;
        }
    }

    public void UsarLogo2()
    {
        foreach (Image logoActual in logos)
        {
            logoActual.sprite = logoV2;
        }
    }

    public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
    {

        if (args == null) args = new object[] { null };
        IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
        jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);
        try
        {
            IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
            if (IntPtr.Zero != returnValue)
            {
                var val = AndroidJNI.GetStringUTFChars(returnValue);
                AndroidJNI.DeleteLocalRef(returnValue);
                return val;
            }
        }
        finally
        {
            AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
        }

        return null;

    }


    private void Start()
    {
        // Si es web local , cargar parametros de configuracion de textos 
        CargarParametrosSessionWeb();
        refSeleccionMouse._DltSeleccion += On_TouchUpReset;
    }

    private void Awake()
    {
#if UNITY_WEBGL
        //Screen.fullScreen = !Screen.fullScreen;
#endif
#if UNITY_STANDALONE_WIN
        //Screen.fullScreen = !Screen.fullScreen;
#endif

#if UNITY_STANDALONE_OSX
        //Screen.fullScreen = !Screen.fullScreen;
#endif

    }

    void OnDisable()
    {
        UnsubscribeEvent();
    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    void UnsubscribeEvent()
    {
        refSeleccionMouse._DltSeleccion -= On_TouchUpReset;
    }

    // Se llama desde el Trigger  cuando el usuario toca una mesa 
    // Se asigna el simulador activo .
    public void TocoMesa(SimuladorGenerico simulador)
    {
        simuladorActivo = simulador;
        control.TocoMesa(simuladorActivo);
        StartCoroutine(pantalla.MostrarTextos(2, simuladorActivo));
        simuladorActivo.intentos = 1;
        pantalla.pantalla_Intentos.text = "1";
    }

    // Si es web local coger los parametros de session por GET 
    private void CargarParametrosSessionWeb()
    {
        if (tipoAplicacion == TipoAplicacion.WEB)
        {
            //Application.ExternalCall("CargarParametrosSession", gameObject.name); hadron games
        }
    }

    public void RecibirPametrosSessionWeb(string parametros)
    {
        string[] arrayParametros = parametros.Split('&');

        usuario = arrayParametros[0];
        curso = arrayParametros[1];


        pantalla.sesion_IdCurso.text = curso;
        pantalla.sesion_usuario.text = usuario;



        /*if (ModoMonoUsuario)
        {
            pantalla.sesion_curso
        }*/

    }

#if UNITY_ANDROID
	void ProcessDirectory(DirectoryInfo aDir)
	{
		var files = aDir.GetFiles().Where(f => f.Extension == ".xml").ToArray();

		foreach(var _fileName in files)
		{
			if(_fileName.Name.Equals("aula_conf.xml"))
            {
				fileName = _fileName.FullName;
				break;
			}
		}
		
		if (fileName.Equals("")) 
        {
			//var subDirs = aDir.GetDirectories ();
			
			//foreach(var subDir in subDirs)
			//{
			//	ProcessDirectory(subDir);
			//}
			//pantalla.checkOptionLang();
		}
		else 
        {
			openXML();
		}
	}
#endif

    /// <summary>
    /// Leer todas las variables del archivo XML
    /// </summary>
    public void openXML()
    {
        try
        {
            XmlDocument newXml = new XmlDocument();
            newXml.Load(fileName);
            XmlNodeList _list = newXml.ChildNodes[0].ChildNodes;

            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].Name.Equals("url_aula"))
                {
                    url_aula = _list[i].InnerText;
                    fileName = fileName + " - " + url_aula;
                }

                if (_list[i].Name.Equals("aula"))
                {
                    Debug.Log(_list[i].InnerText);
                    if (_list[i].InnerText.Equals("true"))
                    {
                        ModoAula = true;
                    }
                    else
                    {
                        ModoAula = false;
                    }

                    fileName = fileName + " - " + ModoAula.ToString();
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("file not exist");
        }
    }

    /// <summary>
    /// Seleccionar la ruta del archivo XML segun la plataforma en donde se ejcuta
    /// </summary>
    public void CargarExternalXML()
    {


        fileName = "";
#if UNITY_IPHONE
		fileName = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		openXML();
        #elif UNITY_ANDROID
            var _dir = "/storage/emulated/0/";
            var _dir0 = "/storage/sdcard/";
            var _dir1 = "/storage/sdcard0/";
        var _dir2 = "/storage/sdcard1/";
        var _dirx = "/sdcard/";
            DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
            DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
            DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
        DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
        DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);
            if(currentDirectory.Exists)
            {
                ProcessDirectory(currentDirectory); 
            }else if(currentDirectory_0.Exists){
                ProcessDirectory(currentDirectory_0); 
        }else if(currentDirectory_1.Exists){
                ProcessDirectory(currentDirectory_1); 
        }else if(currentDirectory_2.Exists){
                ProcessDirectory(currentDirectory_2); 
        }else if (currentDirectory_x.Exists)
        {
            ProcessDirectory(currentDirectory_x);
        }
#elif UNITY_EDITOR
        fileName = Application.dataPath + "/" + "../../aula_conf.xml";
        openXML();
#elif UNITY_STANDALONE_OSX
		fileName = Application.dataPath + "/" + "../../aula_conf.xml"; 
		openXML();
#elif UNITY_STANDALONE_WIN
		fileName = Application.dataPath + "/" + "../../../../aula_conf.xml";
		openXML();
#endif
    }

    /// <summary>
    /// Metodo que se agrega al delegado de la clase Easy touch, el delegado se ejecuta cuando se selecciona un objeto en la habitacion
    /// </summary>
    /// <param name="argBotonseleccionado">Clase que contiene el objecto seleccioando</param>
    void On_TouchUpReset(GameObject argBotonseleccionado)
    {
        if (simuladorActivo.GetType() == typeof(SimuladorAminoAcido) && argBotonseleccionado == btnResetAmino)
        {
            BtnReset(argBotonseleccionado);//ejecuto la funcion del boton reset
        }

        if (simuladorActivo.GetType() == typeof(SimuladorDestilacion) && argBotonseleccionado == btnResetDestilacion)
        {
            BtnReset(argBotonseleccionado);
        }

        if (simuladorActivo.GetType() == typeof(SimuladorFermentacion) && argBotonseleccionado == btnResetFermentacion)
        {
            BtnReset(argBotonseleccionado);
        }
    }

    /// <summary>
    /// Reinicia la simulacion de la estacion a la que pertenece el boton reset presionado
    /// </summary>
    /// <param name="btn">Game object del boton reset presionado</param>
    void BtnReset(GameObject btn)
    {
        if (activeBtnReset && simuladorActivo.usuarioEntroEstacion && !Pantalla.pantallaActiva)
        {
            print("Simulador  RESET ");
            btn.GetComponent<AnimacionBotones>().Click();
            pantalla.BtnReintentar();
        }
    }
}