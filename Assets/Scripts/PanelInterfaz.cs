﻿using UnityEngine;
using System.Collections;

public class PanelInterfaz : MonoBehaviour
{
    public GameObject panel;
    bool animarEntrada = false;
    // Use this for initialization
    public CanvasGroup uiPanel;

    void Awake()
    {
        uiPanel = panel.GetComponent<CanvasGroup>();

        if (panel && uiPanel != null)
        {
            uiPanel.alpha = 0;
        }
    }

    void Update()
    {
        if (animarEntrada && uiPanel != null)
        {
            uiPanel.alpha += Time.deltaTime * 2;
            if (uiPanel.alpha >= 1)
            {
                uiPanel.alpha = 1;
                animarEntrada = false;
            }
        }
    }

    public void AbrirPanel()
    {
        panel.SetActive(true);

        if (panel.GetComponent<CanvasGroup>() != null)
        {
            animarEntrada = true;
        }
    }

    public void CerrarPanel()
    {
        if (panel.GetComponent<CanvasGroup>() == null)
        {
            panel.SetActive(false);
        }
        else
        {
            uiPanel.alpha = 0;
            panel.SetActive(false);
        }
    }
}
