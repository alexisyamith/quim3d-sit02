﻿using UnityEngine;

public class TriggerMesa : MonoBehaviour
{
    // se selecciona desde el editor el tipo
    //public Simulador.TipoSimulador tipo;
    public Simulador simulador;

    public SimuladorGenerico simuladorActivo;

    private void OnTriggerEnter(Collider colision)
    {
        if (colision.gameObject.tag == "Player")
            simulador.TocoMesa(simuladorActivo);
    }
}