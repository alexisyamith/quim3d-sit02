using UnityEngine;

/// <summary>
/// Clase usada para cambiar los tiempos de simulacion y probar las cosas mas rapido
/// </summary>
public class EscalaTiempo : MonoBehaviour
{

#if UNITY_EDITOR
    private void OnGUI()
    {
        var pos = new Rect(0, 0, 100, 25);

        if (GUI.Button(pos, "Tiempo  1x"))
        {
            Time.timeScale = 1;
        }

        pos.x += 101;

        if (GUI.Button(pos, "Tiempo  10x"))
        {
            Time.timeScale = 15;
        }

        pos.x = 0;
        pos.y = 50;
    }
#endif
}