﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using SimpleJSON;

public class RecepcionDatosWebAula : MonoBehaviour {


    [SerializeField] private Simulador seguridad;
    /// <summary>
    /// [0] nombre de usuario
    /// [1] contraseña
    /// </summary>
    [SerializeField] private InputField[] inputLoguin2campos;
    [SerializeField] private string webGLcodigoSituacion;



public void ObtenerDatosWEbAula() { 
#if UNITY_WEBGL
            if (seguridad.ModoAula)
            {

                Debug.Log("entra Web aula");


                inputLoguin2campos[0].enabled = false;
                inputLoguin2campos[1].enabled = false;

                string urlWebGL = seguridad.WebGLUrl;
                string webGLCode = webGLcodigoSituacion;


                WWWForm form = new WWWForm();
                form.AddField("codigo", webGLCode);

                WWW wwwAulaWebGL = new WWW(urlWebGL, form);
                StartCoroutine(WaitForRequestAulaWebGL(wwwAulaWebGL));

            }
#endif

}

//rtrieving the data
private IEnumerator WaitForRequestAulaWebGL(WWW www)
{
    yield return www;
    if (www.error == null)
    {
        try
        {
            Debug.Log(www.text);
            var dataReceived = JSON.Parse(www.text);
            Debug.Log("DataWebAULA: " + dataReceived); ;
            if (dataReceived["RESULT"] != null)
            {
                inputLoguin2campos[0].text = dataReceived["RESULT"]["email"].Value;
                inputLoguin2campos[1].text = dataReceived["RESULT"]["password"].Value;
            }
        }
        catch (Exception error)
        {
            Debug.Log(error);
        }
    }
}

}
