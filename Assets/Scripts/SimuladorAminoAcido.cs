﻿
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/*
	Resumen : 

	El objetivo es crear una mezcla de aminoacidos.

	Paso1  :  Solucion1  +  solvente  =  ingresar en el reactor   
	Paso2  :  Agitar
	Paso3  : Pasar mezcla al frasco 2 del reactor 
	Paso4 :  Solucion2 +  solvente  =  ingresar en el reactor   
	Paso5  :  Agitar
	Paso6  :  Combinar mezclas 1 y 2   , agitar y calentar 
	Paso7  :  Sacar del reactor y llevar al frasco de pruebas .


	Cantidad de aminoácido a preparar: 50ml con porcentajes del 40% de A y 60% de C
	Insumos que se encuentran disponibles para hacer las combinaciones: 
	Solución A
	Solución B 
	Solución C 
	Solución D 

	Detalle simulacion 

	Req  1 
    Verificar que los reactivos estén listos para usarse (tengan líquido) : 
	Logica :  Poner un boton que cambie de color en x segundos cuando este listo 

	Req  2 
	Verificar el solvente para acondicionamiento de los aminoácidos
	Logica :  Poner un boton que cambie de color en x segundos cuando este listo 

	Req3  
	Dosificar volumen de solución A 
	Logica : Abrir Valvula de sol A.  ( ingresa dinamicamente ?) 
	Adiciona Solucion y volvente  a la vez ? 

	Agitar mezcla :  Boton para inciar agitacion  ( para automatico  ? ) 

	Req3 
	Adicionar los insumos al tanque de proceso. 
	Logica : Se abre valvula que permite pasar del tanque 1 al tanque de proceso  

	Req4 
	Se adiciona cada reactivo acondicionado al tanque de proceso
	Realizar el mismo proceso con el solucion b o otra solucion

	Req5  igual al 3 
	Adicionar los insumos al tanque de proceso. 
	Logica : Se abre valvula que permite pasar del tanque 1 al tanque de proceso  

	Req6
	Agitar ? 

	Req7
	Llevar mezcla a rep final 

	Req8 
	Probar mezcla


	 */

/// <summary>
///  Clase simulador de amino acidos , encargada del funcionamiento de esta unidad
/// </summary>
public class SimuladorAminoAcido : SimuladorGenerico
{
    public Transform mixer;
    public Vector3 shakeAxis;
    float shakeRot = 0;
    float shakeTime;
    public bool ishaking = false;
    private bool Shaking1 = false;
    private bool Shaking2 = false;
    private bool Shaking3 = false;

    // Tipos de soluciones 
    public enum SolutionType { solA, solB, solC, solD, solvent, other };
    SolutionType solutionType;

    // References to ingame buttons 
    public GameObject btnSolA;
    public GameObject btnSolB;
    public GameObject btnSolC;
    public GameObject btnSolD;
    public GameObject btnSolvent;
    public GameObject btnIncreaseRpm;
    public GameObject btndDecreaseRpm;
    public GameObject btnAddTemp;
    public GameObject btndMinusTemp;
    public GameObject btnShake;
    public GameObject btnSendSolToVessel2;
    public GameObject btnSendSolToFinalVessel;
    public GameObject btnFinalTest;
    public GameObject btnDigitalScreen;
    public GameObject btnReset;

    // RPMS
    public int rpm = 0;
    int maximoRpm = 200;

    // Cantidaddes de soluciones que se ingresan en el recipiente de acondicionamiento
    int valveSolFactor = 10;
    public int temperature = 23;

    // Diccionario de funciones para los clic en el panel general 
    Dictionary<GameObject, Action> escenaClic = new Dictionary<GameObject, Action>();

    // Vessel 1  ( right) 
    public Vessel vessel1;
    // Vessel 2  ( left) 
    public Vessel vessel2;
    // Vessel 3 final test vessel 
    public Vessel finalVessel;

    public Vessel solA;
    public Vessel solB;
    public Vessel solC;
    public Vessel solD;
    public Vessel solvent;

    public float error = 0;
    private float shakeTimeForStop = 0;
    private int errorSolvent = 10;
    private int errorShaking = 10;
    private int errorOmitShaking = 10;
    private int errorTemperature = 10;

    //Indicators
    private float synthesisTest;
    public float aminoAcidProcess;
    public float conditioningMP;

    //Operation
    private int lastRPM;
    private int lastTemperature;
    private bool firstAddedSolution;
    public static bool sendingContent;

    //Colors
    private Material materialvessel1;
    private Material materialvessel2;
    private Material materialvesselFinal;
    private Color colorInitialVessel = Color.white;

    //Lerp  Color Vessel
    private Renderer colorPresentVessel2;
    private Renderer colorPresentVesselFinal;
    private Color colorOptimal = Color.red;
    private Color colorWrong = Color.black;
    private float timeLerp;

    //Delegates
    public delegate void UpdateStates();
    public UpdateStates state;

    //Solution Ready 
    public Renderer[] pilotSolutionReady;
    private Material materialSolutionReady;
    private Material materialSolutionNoReady;

    //Messages
    private string msjSolutionNoReady = "La solución aún no esta disponible.";
    private string msjSolutionReady = "Las soluciones ya se encuentran disponibles.";
    private string msjMaxCapacity = "No se puede agregar mas contenido.";

    //Reference Animation Btn Script
    private AnimacionBotones scriptBtnShake;
    private AnimacionBotones scriptBtnRPMAdd;
    private AnimacionBotones scriptBtnRPMRemove;
    private AnimacionBotones scriptBtnTempAdd;
    private AnimacionBotones scriptBtnTempRemove;
    private AnimacionBotones scriptBtnSendVessel2;
    private AnimacionBotones scriptBtnSendFinalConteiner;
    private AnimacionBotones scriptBtnFinalTest;

    public struct DatosGrafico
    {
        public float tiempo;
        public float temperatura;
    };

    List<DatosGrafico> datosGrafico;

    // Use this for initialization
    void Start()
    {
        datosGrafico = new List<DatosGrafico>();

        msjSolutionNoReady = controlIdiomas.Traductor(msjSolutionNoReady);
        msjSolutionReady = controlIdiomas.Traductor(msjSolutionReady);
        msjMaxCapacity = controlIdiomas.Traductor(msjMaxCapacity);

        ChangeState(State_NoActivity);
        colorPresentVessel2 = vessel2.vesselModel.GetComponent<Renderer>();
        colorPresentVesselFinal = finalVessel.vesselModel.GetComponent<Renderer>();
        materialSolutionNoReady = pilotSolutionReady[0].material;
        materialSolutionReady = pilotSolutionReady[1].material;
        materialvessel1 = vessel1.vesselModel.GetComponent<Renderer>().material;
        materialvessel2 = vessel2.vesselModel.GetComponent<Renderer>().material;
        materialvesselFinal = finalVessel.vesselModel.GetComponent<Renderer>().material;

        refSeleccionMouse._DltSeleccion += On_TouchUpReset;
        refSeleccionMouse._DltSeleccion += On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion += On_TouchUpBtnControlGeneral;

        // amarrar los objetos a una funcion 
        escenaClic.Add(btnSolA, Clic_SolutionA);
        escenaClic.Add(btnSolB, Clic_SolutionB);
        escenaClic.Add(btnSolC, Clic_SolutionC);
        escenaClic.Add(btnSolD, Clic_SolutionD);
        escenaClic.Add(btnSolvent, Clic_btnSolvent);

        escenaClic.Add(btnIncreaseRpm, Clic_btnIncreaseRpm);
        escenaClic.Add(btndDecreaseRpm, Clic_btndDecreaseRpm);

        escenaClic.Add(btnAddTemp, Clic_btnAddTemp);
        escenaClic.Add(btndMinusTemp, Clic_btndMinusTemp);

        escenaClic.Add(btnShake, Clic_btnShake);
        // change of vessels 
        escenaClic.Add(btnSendSolToVessel2, Clic_btnSendSolToVessel2);
        escenaClic.Add(btnSendSolToFinalVessel, Clic_btnSendSolToFinalVessel);
        escenaClic.Add(btnFinalTest, Clic_btnFinalTest);

        //Assing reference to buttons
        scriptBtnShake = btnShake.GetComponent<AnimacionBotones>();
        scriptBtnRPMAdd = btnIncreaseRpm.GetComponent<AnimacionBotones>();
        scriptBtnRPMRemove = btndDecreaseRpm.GetComponent<AnimacionBotones>();
        scriptBtnTempAdd = btnAddTemp.GetComponent<AnimacionBotones>();
        scriptBtnTempRemove = btndMinusTemp.GetComponent<AnimacionBotones>();
        scriptBtnSendVessel2 = btnSendSolToVessel2.GetComponent<AnimacionBotones>();
        scriptBtnSendFinalConteiner = btnSendSolToFinalVessel.GetComponent<AnimacionBotones>();
        scriptBtnFinalTest = btnFinalTest.GetComponent<AnimacionBotones>();

        InitializeValues();
    }

    //Initialize values
    private void InitializeValues()
    {
        //Pilots
        foreach (Renderer _pilotSolutionReady in pilotSolutionReady)
            _pilotSolutionReady.material = materialSolutionNoReady;

        //Solutions
        Reset();
        simulador.pantalla.screen_solutionA.text = "0";
        simulador.pantalla.screen_solutionB.text = "0";
        simulador.pantalla.screen_solutionC.text = "0";
        simulador.pantalla.screen_solutionD.text = "0";
        simulador.pantalla.screen_solvent.text = "0";
        simulador.pantalla.input_solutionA.text = "0";
        simulador.pantalla.input_solutionB.text = "0";
        simulador.pantalla.input_solutionC.text = "0";
        simulador.pantalla.input_solutionD.text = "0";
        simulador.pantalla.input_solvent.text = "0";

        //Temperature
        temperature = 23;
        simulador.pantalla.input_temperature.text = temperature.ToString();
        simulador.pantalla.screen_temperature.text = temperature.ToString();

        //Agitator
        rpm = 0;
        simulador.pantalla.input_agitatorSpeed.text = rpm.ToString();
        simulador.pantalla.screen_agitator.text = rpm.ToString();

        //Synthesis Test
        error = 0;
        simulador.pantalla.indicator_synthesisTest.text = error.ToString();

        //AminoAcid Process
        simulador.pantalla.indicator_aminoAcidProcess.text = "0";

        //ConditioningMP
        simulador.pantalla.indicator_conditioningMP.text = "0";

        //Color
        materialvessel1.color = colorInitialVessel;
        materialvessel2.color = colorInitialVessel;
        materialvesselFinal.color = colorInitialVessel;

        //Checking solutions
        firstAddedSolution = true;
        Shaking1 = false;
        Shaking2 = false;
        Shaking3 = false;

        //Vessel contents liquid
        vessel1.totalLiquid = 0;
        vessel1.InitializeSize();
        vessel2.totalLiquid = 0;
        vessel2.Restart();
        finalVessel.totalLiquid = 0;
        finalVessel.InitializeSize();
        sendingContent = false;

        StartCoroutine(DeactiveBtnDown());

        ishaking = false;
        ReiniciarGrafica();
    }

    void ReiniciarGrafica()
    {

        datosGrafico = new List<DatosGrafico>();
        // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0 });
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0 });
    }

    //Delay to prevent bug with Click funcions
    IEnumerator DeactiveBtnDown()
    {
        yield return new WaitForSeconds(0.1f);
        scriptBtnShake.Normalizar();
        scriptBtnFinalTest.Normalizar();
    }

    #region Region_EasyTouch
    void On_TouchUpBtnControlGeneral(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject)
        {
            if (argBotonseleccionado != null)
            {
                // Ejecutar la accion para este boton del panel
                if (escenaClic.ContainsKey(argBotonseleccionado))
                {
                    escenaClic[argBotonseleccionado].Invoke();
                    simulador.pantalla.DibujarGrafico(false);
                }
            }
        }
    }

    void On_TouchUpReset(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject && usuarioEntroEstacion)
        {
            if (argBotonseleccionado == btnReset)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada && Simulador.activeBtnReset)
                {
                    print("Simulador.activeBtnReset: " + Simulador.activeBtnReset);
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.BtnReintentar();
                    ReiniciarGrafica();
                }
            }
        }
    }

    void On_TouchUpPantalla(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject)
        {
            if (argBotonseleccionado == btnDigitalScreen)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
                {
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.AccionarPantalla();
                    simulador.pantalla.ActualizarDatosPantalla();
                }
            }
        }
    }

    public override void MostrarPantalla()
    {
        if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
        {
            simulador.pantalla.AccionarPantalla();
            simulador.pantalla.ActualizarDatosPantalla();
            return;
        }
        else
            print("cierre la pantalla o inicie el simulador");
    }

    void OnDisable()
    {
        UnsubscribeEvent();
    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    void UnsubscribeEvent()
    {
        refSeleccionMouse._DltSeleccion -= On_TouchUpBtnControlGeneral;
        refSeleccionMouse._DltSeleccion -= On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion -= On_TouchUpReset;
    }
    #endregion

    // Update is called once per frame
    void Update()
    {
        state();
    }

    void Reset()
    {
        vessel1.Empty();
        vessel2.Empty();
        finalVessel.Empty();


    }
    // se debe girar el agitador con rpm y configurarlo para que rote en cualquier orientacion
    void StartShake()
    {
        shakeTime = 0;
        shakeRot = 0;
    }

    // RPM  =  150 vueltas por segundo 
    // Revisar ese numero con Joan
    void Shake(int revbyMin)
    {
        // En 1 segundo  va de hace 1 ciclo 
        // 1 segundo por media vuelta 
        shakeTime += Time.deltaTime;
        shakeRot = Mathf.PingPong(shakeTime * 2 * revbyMin / 60, 1);
        shakeRot = Mathf.Lerp(-1, 1, shakeRot) * 45;
        mixer.localEulerAngles = shakeAxis * shakeRot;
    }

    void Clic_SolutionA()
    {
        if (state == State_Simulation)
        {
            if (vessel1.totalLiquid >= vessel1.maxVolume)
                simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
            else
                solA.SendSolution(vessel1, valveSolFactor);
        }
        else
            simulador.pantalla.MostrarMensajeFermentacion(msjSolutionNoReady);
    }

    void Clic_SolutionB()
    {
        if (state == State_Simulation)
        {
            if (vessel1.totalLiquid >= vessel1.maxVolume)
                simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
            else
                solB.SendSolution(vessel1, valveSolFactor);
        }
        else
            simulador.pantalla.MostrarMensajeFermentacion(msjSolutionNoReady);
    }

    void Clic_SolutionC()
    {
        if (state == State_Simulation)
        {
            if (vessel1.totalLiquid >= vessel1.maxVolume)
                simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
            else
                solC.SendSolution(vessel1, valveSolFactor);
        }
        else
            simulador.pantalla.MostrarMensajeFermentacion(msjSolutionNoReady);
    }

    void Clic_SolutionD()
    {
        if (state == State_Simulation)
        {
            if (vessel1.totalLiquid >= vessel1.maxVolume)
                simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
            else
                solD.SendSolution(vessel1, valveSolFactor);
        }
        else
            simulador.pantalla.MostrarMensajeFermentacion(msjSolutionNoReady);
    }

    void Clic_btnSolvent()
    {
        if (state == State_Simulation)
        {
            if (vessel1.totalLiquid >= vessel1.maxVolume)
                simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
            else
                solvent.SendSolution(vessel1, valveSolFactor);
        }
        else
            simulador.pantalla.MostrarMensajeFermentacion(msjSolutionNoReady);
    }

    void Clic_btnIncreaseRpm()
    {
        rpm = rpm < maximoRpm ? rpm + 10 : maximoRpm;
        simulador.pantalla.input_agitatorSpeed.text = rpm.ToString();
        scriptBtnRPMAdd.StopCoroutine("ActivarBtInstantaneamente");
        scriptBtnRPMAdd.StartCoroutine("ActivarBtInstantaneamente");
    }

    void Clic_btndDecreaseRpm()
    {
        rpm = rpm > 0 ? rpm - 10 : rpm;
        simulador.pantalla.input_agitatorSpeed.text = rpm.ToString();
        scriptBtnRPMRemove.StopCoroutine("ActivarBtInstantaneamente");
        scriptBtnRPMRemove.StartCoroutine("ActivarBtInstantaneamente");
    }

    void Clic_btnAddTemp()
    {
        temperature = temperature < 100 ? temperature + 1 : temperature;
        simulador.pantalla.input_temperature.text = temperature.ToString();
        scriptBtnTempAdd.StopCoroutine("ActivarBtInstantaneamente");
        scriptBtnTempAdd.StartCoroutine("ActivarBtInstantaneamente");
    }

    void Clic_btndMinusTemp()
    {
        temperature = temperature > 23 ? temperature - 1 : temperature;
        simulador.pantalla.input_temperature.text = temperature.ToString();
        scriptBtnTempRemove.StopCoroutine("ActivarBtInstantaneamente");
        scriptBtnTempRemove.StartCoroutine("ActivarBtInstantaneamente");
    }

    void Clic_btnShake()
    {
        StartShake();
        ishaking = !ishaking;
        scriptBtnShake.Click(ishaking);
    }

    void Clic_btnSendSolToVessel2()
    {

        if (ishaking || vessel1.totalLiquid == 0 || sendingContent)
            return;
        if ((vessel2.totalLiquid + vessel1.totalLiquid) > vessel2.maxVolume)
            simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
        else
            AddSolutionToVessel2();
    }

    void Clic_btnSendSolToFinalVessel()
    {

        if (ishaking || sendingContent)
            return;
        if ((finalVessel.totalLiquid + vessel2.totalLiquid) > finalVessel.maxVolume)
            simulador.pantalla.MostrarMensajeFermentacion(msjMaxCapacity);
        else
        {
            SendToFinalContainer();
            scriptBtnSendFinalConteiner.StopCoroutine("ActivarBtInstantaneamente");
            scriptBtnSendFinalConteiner.StartCoroutine("ActivarBtInstantaneamente");
        }
    }

    private void Clic_btnFinalTest()
    {
        if (finalVessel.totalLiquid > 0)
        {
            //Deactivate buttons for evit wrong functionality
            Simulador.activeBtnReset = false;
            CheckSimulation();
            scriptBtnFinalTest.Click();
        }
    }

    // Adicionar insumos al tanque de proceso , pasa de tanque 1 al tanque 2
    void AddSolutionToVessel2()
    {
        sendingContent = true;
        //Parameters Check solvent & solution

        if (firstAddedSolution)
            vessel1.solutionType = SolutionType.solA;
        else
            vessel1.solutionType = SolutionType.solC;

        CheckSolventValues(vessel1);
        CheckSolutionValues(vessel1);


        CheckShakingInVessel1();
        vessel1.SendAllLiquid(vessel2);
        timeLerp = 0;
        StartCoroutine(ChangeColorVeseel(colorPresentVessel2, vessel2.vesselModel.GetComponent<Renderer>().material.color, vessel1.vesselModel.GetComponent<Renderer>().material.color));
        firstAddedSolution = false;
        scriptBtnSendVessel2.StopCoroutine("ActivarBtInstantaneamente");
        scriptBtnSendVessel2.StartCoroutine("ActivarBtInstantaneamente");
    }

    // Send fluid to the final container for test	
    void SendToFinalContainer()
    {
        sendingContent = true;
        CheckShakingInVessel2();
        CheckTemperatureValue();
        vessel2.SendAllLiquid(finalVessel);
        timeLerp = 0;
        StartCoroutine(ChangeColorVeseel(colorPresentVesselFinal, finalVessel.vesselModel.GetComponent<Renderer>().material.color, vessel2.vesselModel.GetComponent<Renderer>().material.color));
    }

    //Active pilots of solution ready
    void SolutionReady()
    {
        foreach (Renderer _pilotSolutionReady in pilotSolutionReady)
            _pilotSolutionReady.material = materialSolutionReady;
        ChangeState(State_Simulation);
        simulador.pantalla.ejecutandoEjercicio = true;
        Simulador.activeBtnReset = true;

        //Display message active solutions
        StartCoroutine(simulador.pantalla.DesactivarMensajeFermentacion(0, 0));
        simulador.pantalla.MostrarMensajeFermentacion(msjSolutionReady, 1);
    }

    #region States
    //Change State
    private void ChangeState(UpdateStates _state)
    {
        state = _state;

    }

    //State Begin
    void State_BeginSimulation()
    {
        ContarTiempoSession();
    }

    //State Simulation
    void State_Simulation()
    {


        ejecutoVariosEjercicios = true;
        ContarTiempoSession();

        if (ishaking)
        {
            Shake(rpm);
            shakeTimeForStop = 0;
            lastRPM = rpm;
            lastTemperature = temperature;
        }
        else
        {
            if (shakeTimeForStop < 1)
            {
                shakeTimeForStop += Time.deltaTime;

                float angulo = Mathf.LerpAngle(mixer.localEulerAngles.magnitude, 0, shakeTimeForStop);
                //if ( angulo > 360 ) angulo = angulo -360;
                mixer.localEulerAngles = shakeAxis * angulo;
            }
        }

        // si es el primer dato 
        if (datosGrafico.Count == 0)
        {
            ReiniciarGrafica();
        }

        if (!Mathf.Approximately(datosGrafico[datosGrafico.Count - 1].temperatura, temperature))
        {
            // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
            datosGrafico.Add(new DatosGrafico() { tiempo = segundosSesion, temperatura = temperature });
            datosGrafico.Add(new DatosGrafico() { tiempo = segundosSesion, temperatura = temperature });
        }
        else
        {
            DatosGrafico dato = datosGrafico[datosGrafico.Count - 1];
            dato.tiempo = segundosSesion;
            datosGrafico[datosGrafico.Count - 1] = dato;
        }
    }

    //State No Activity
    private void State_NoActivity()
    {

    }
    #endregion

    // Resultado final depende de 
    /*
	
	Porcentajes de A y C 
	
	Si el volumen de solvente no corresponde a la cantidad especifica  se le coloca un 10% de error.
	Si las cantidades no se mezclan en proporciones adecuadas el % de error se rebaja al 100%


	temp > 50 se daña el aminoacido
	temp ambiente = error del 10% 
	Si la temperatura es menor  el error es del  10%
	temperatura varia de 20 a 100 grados .

	Color optimo: Rojo
	Color error : Negro 
	Color de A: Amarillo
	Color de B: Azul 
	Color de C: Verde
	Color de D: Morado
	 */

    // Revisar si el valor del solvente  A  y B son correctos 
    void CheckSolventValues(Vessel v)
    {
        if (v.solutionType == SolutionType.solA)
        {
            if (v.solA != v.solvent)
            {
                error += errorSolvent;
                print("error: solvente diferente de A");
                print("Penalizacion + : " + errorSolvent);
            }
        }
        if (v.solutionType == SolutionType.solC)
        {
            if (v.solC != v.solvent)
            {
                error += errorSolvent;
                print("error: solvente diferente de C");
                print("Penalizacion + : " + errorSolvent);
            }
        }
    }

    private bool firstPass = false;
    private bool secondPass = false;

    void CheckSolutionValues(Vessel v)
    {
        if (v.solutionType == SolutionType.solB || v.solutionType == SolutionType.solD)
        {
            error = 100;
            print("error: solucion diferente a A y C");
            print("Penalizacion + : " + 100);
            return;
        }
        if (v.solutionType == SolutionType.solA)
        {
            if (v.solA != 30)
            {
                float errorLiquido = Mathf.Abs(100 * (30 - v.solA) / 30);
                error += errorLiquido;
                print("Error: solucion A diferente a 30");
                print("Penalizacion: " + errorLiquido);
            }
            return;
        }
        if (v.solutionType == SolutionType.solC)
        {
            if (v.solC != 20)
            {
                float errorLiquido = Mathf.Abs(100 * (20 - v.solC) / 20);
                error += errorLiquido;
                print("Error: solucion C diferente a 20");
                print("Penalizacion + : " + errorLiquido);
            }
            return;
        }
    }

    void CheckTemperatureValue()
    {
        if (temperature > 45)
        {
            error += 100;
            print("error temperatura ");
            print("Penalizacion + : " + 100);

        }
        if (temperature < 35)
        {
            error += errorTemperature;
            print("Error temperatura ");
            print("Penalizacion + : " + errorTemperature);
        }

    }

    // si no hay mezclador el porcentaje es del 5% menor en cada agitación omitida o con valores no adecuados
    // rpm 100 en los reactivos
    // rpm  150 en el frasco proceso 
    void CheckShakingInVessel1()
    {
        if (lastRPM != 100)
        {
            error += errorShaking;
            print("Error RPM ");
            print("Penalizacion + : " + errorShaking);
        }
        else
        {
            if (firstAddedSolution)
                Shaking1 = true;
            else
                Shaking2 = true;
        }
        lastRPM = 0;
    }

    void CheckShakingInVessel2()
    {
        if (lastRPM != 150)
        {
            error += errorShaking;
            print("Error RPM ");
            print("Penalizacion + : " + errorShaking);
        }
        else
            Shaking3 = true;

        lastRPM = 0;
    }

    void CheckSimulation()
    {
        if (!Shaking1)
            error += 20;
        if (!Shaking2)
            error += 20;
        if (!Shaking3)
            error += 20;
        if (error > 100)
            error = 100;

        print("Error " + error);

        synthesisTest = 100 - error;
        simulador.pantalla.indicator_synthesisTest.text = synthesisTest.ToString();
        //Changes color according to procedure

        timeLerp = 0;
        if (error > 0)
            StartCoroutine(ChangeColorVeseel(colorPresentVesselFinal, finalVessel.vesselModel.GetComponent<Renderer>().material.color, colorWrong, true, false));
        else
            StartCoroutine(ChangeColorVeseel(colorPresentVesselFinal, finalVessel.vesselModel.GetComponent<Renderer>().material.color, colorOptimal, true, true));
    }

    //Changes color in vessel
    private IEnumerator ChangeColorVeseel(Renderer _colorPresent, Color colorInitial, Color colorTerminal, bool lerpFinal = false, bool resultadoEjercicio = true)
    {
        yield return new WaitForSeconds(0.005f);
        _colorPresent.material.color = Color.Lerp(colorInitial, colorTerminal, timeLerp);
        timeLerp += Time.deltaTime;
        if (timeLerp <= 1)
            StartCoroutine(ChangeColorVeseel(_colorPresent, colorInitial, colorTerminal, lerpFinal, resultadoEjercicio));
        else
        {
            if (lerpFinal)
            {
                yield return new WaitForSeconds(1);

#if MODO_RAPIDO
				resultadoEjercicio = true;
#endif
                simulador.pantalla.MostrarResultadoEjercicio(resultadoEjercicio);


                simulador.pantalla.panelEntradaDatos.gameObject.SetActive(false);
            }
        }
    }

    //Is called from the hud when you press start
    public override void BtnIniciar(bool _practicaLibre)
    {
        practicaLibre = _practicaLibre;
        Simulador.ejecutandoEjercicio = false;
        Invoke("SolutionReady", 3);
        ChangeState(State_BeginSimulation);
        InitializeValues();
        StartCoroutine(simulador.pantalla.DesactivarMensajeFermentacion(0, 0));
        StartCoroutine(simulador.pantalla.DesactivarMensajeFermentacion(1, 0));
        usuarioEntroEstacion = true;
        if (!_practicaLibre)
        {
            ReiniciarGrafica();
        }
    }

    //When the user exits the simulator
    public void SalidaAminoAcido()
    {
        mixer.localEulerAngles = Vector3.zero;
        ChangeState(State_NoActivity);
        InitializeValues();
    }

    //Get data for the graphic 	
    public override float[] ObtenerDatosGrafica()
    {
        float[] _data = new float[datosGrafico.Count * 2];
        int i = 0;
        foreach (var datoGrafico in datosGrafico)
        {
            _data[i] = datoGrafico.tiempo;
            _data[datosGrafico.Count + i] = datoGrafico.temperatura;
            i++;
        }
        return _data;
    }

    //Questions from external file
    public void ReceiveQuestionsSimulator(string _externalQuestions)
    {
        externalQuestions = _externalQuestions.Split('&');
        for (int i = 0; i < externalQuestions.Length; i++)
            refExternalQuestions.Add(externalQuestions[i]);
    }

    //Refresh the report data
    public override void ActualizarDatosReporte()
    {
        ChangeState(State_NoActivity);
        tablaDatos.Clear();

        tablaDatos.Add(finalVessel.solA.ToString() + " ml");
        tablaDatos.Add(finalVessel.solB.ToString() + " ml");
        tablaDatos.Add(finalVessel.solC.ToString() + " ml");
        tablaDatos.Add(finalVessel.solD.ToString() + " ml");
        tablaDatos.Add(finalVessel.solvent.ToString() + " ml");
        tablaDatos.Add(lastTemperature.ToString("f0") + "");
        tablaDatos.Add(rpm.ToString("f0") + " rpm");
        tablaDatos.Add(simulador.pantalla.indicator_conditioningMP.text + " ml");
        tablaDatos.Add(simulador.pantalla.indicator_aminoAcidProcess.text + " ml");
        tablaDatos.Add(synthesisTest.ToString("f2") + " %");

        simulador.pantalla.reporte_Fecha.text = System.DateTime.Now.ToString("dd-MM-yyyy");

        simulador.simuladorActivo.reporte.GuardarTablaDatos(tablaDatosTextos, tablaDatos);
        simulador.pantalla.textoGrafica.text = nombreGrafica;
        simulador.simuladorActivo.reporte.GuardarNombrePDF(nombrePDF);
    }

    //Get dato for the Digital Screen
    public float[] GetDataScreen()
    {
        float[] data = new float[5];
        data[0] = vessel1.solA + vessel2.solA + finalVessel.solA;
        data[1] = vessel1.solB + vessel2.solB + finalVessel.solB;
        data[2] = vessel1.solC + vessel2.solC + finalVessel.solC;
        data[3] = vessel1.solD + vessel2.solD + finalVessel.solD;
        data[4] = vessel1.solvent + vessel2.solvent + finalVessel.solvent;
        return data;
    }
}
