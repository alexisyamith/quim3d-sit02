using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using SimpleJSON;
using NSCapturaPantalla;
using NSScore;
#if !UNITY_WEBPLAYER
using System.Net.NetworkInformation;
#endif
using Utilities;
#if !UNITY_WEBGL
using ValidacionMenu;
#endif

/// <summary>
/// Clase que controla y modifica los valores que se muestran en la interfaz
/// </summary>
public class Pantalla : MonoBehaviour
{
    public RecepcionDatosWebAula claseWebAula;
    public GameObject PanelSelIdioma;
    public GameObject PanelSelLanguage_ENG_POR;
    public bool SelLangOption;
    public string pairLanguages;

    public bool var_seguridad;
    private bool var_aula;
    public bool var_offline = false;

    public Camera GUICamera;

    //Testing seguridad 2
    public Text testSeguridad;

        //Textos adicionados
    public Text textoFormula;

    //FERMENTACION

    [Header("Fermentacion"), Space(10)]
    //Datos Entrada 
    public Text azucarTexto;
    public Text levaduraTexto;
    public Text acidoTexto;
    public Text baserTexto;
    public Text rpmTexto;
    public Text temperaturaTexto;
    //Datos Pantalla Digital
    public Text pantalla_Azucar;
    public Text pantalla_Levadura;
    public Text pantalla_Acido;
    public Text pantalla_Base;
    public Text pantalla_Temperatura;
    public Text pantalla_Rpm;
    //Datos Indicadores 
    public Text indicadores_PH;
    public Text indicadores_Presion;
    public Text indicadores_Porcentaje_Alcohol;
    public Text indicador_Tiempo_Usuario;
    public Text indicadores_Nivel_Cultivo_Texto;
    public Image indicadores_Nivel_Cultivo_Color;
    //Mensajes 
    public GameObject[] textoMensajeFermentacion;

    [Header("Destilacion"), Space(10)]

    //DESTILACION
    //Datos Entrada 
    public Text solucionDestilacionTexto;
    public Text acidoDestilacionTexto;
    public Text baserDestilacionTexto;
    public Text temperaturaDestilacionTexto;
    //Datos Pantalla Digital
    public Text pantalla_Solucion_Destilacion;
    public Text pantalla_Acido_Destilacion;
    public Text pantalla_Base_Destilacoin;
    public Text pantalla_Temperatura_Destilacion;
    //Datos Indicadores 
    public Text indicadores_Alcohol_Destilacion;
    public Text indicadores_PH_Destilacion;
    public Text indicador_Tiempo_Usuario_Destilacion;
    public Text indicadores_Nivel_Agua_Destilacion_Texto;

    [Header("Aminoacidos"), Space(10)]

    //AMINO ACIDS
    //Data Entry Data 
    public Text input_solutionA;
    public Text input_solutionB;
    public Text input_solutionC;
    public Text input_solutionD;
    public Text input_solvent;
    public Text input_temperature;
    public Text input_agitatorSpeed;
    //Data Screen 
    public Text screen_solutionA;
    public Text screen_solutionB;
    public Text screen_solutionC;
    public Text screen_solutionD;
    public Text screen_solvent;
    public Text screen_temperature;
    public Text screen_agitator;
    //Data Indicator 
    public Text indicator_conditioningMP;
    public Text indicator_aminoAcidProcess;
    public Text indicator_synthesisTest;

    //Paneles y contenido pantalla digital
    public GameObject panelPantalla;
    public GameObject panelGraficoAminoacido;
    public GameObject panelGraficoDestilacion;
    public GameObject panelGraficoFermentacion;
    public GameObject panelCalculadora;
    public GameObject panelEcuaciones;
    public GameObject panelInstrucciones;
    public Button btnCalculadora;
    public Button btnEcuaciones;
    public Button btnInstrucciones;
    public Grafico2D grafico2D;

    public Text pantalla_Tiempo;
    public Text Datos_Usuario;
    public Text pantalla_Intentos;

    //Paneles Menu
    //public GameObject panelHud;
    public GameObject panelInicial;
    public GameObject panelEjercicios;
    public GameObject panelPortada;
    public GameObject panelEvaluacion;
    public GameObject panelMensajes;
    public GameObject panelEntradaDatos;
    public GameObject panelInicioSesion;
    public GameObject panelReporte;
    public GameObject panelIntentosReloj;
    public GameObject panelInformacion;

    //Textos Menu
    public Text titulo;
    public Text textoSimulacion;
    public Text labelRetoPractica;
    public List<Text> textoEvaluacion;
    public Text textoMensaje;
    //public UILabel advertenciaSalir;
    public Text textoInformacion;

    //Sesion
    [Header("Login"), Space(10)]
    public InputField sesion_usuario;
    public InputField sesion_IdCurso;
    public InputField sesion_curso;
    public InputField sesion_Institucion;

    public GameObject sesion_IdCurso_label;
    public Text sesion_curso_label;
    public GameObject sesion_Institucion_label;
    public Text sesion_mensaje_error;

    public float _cal = 0;

    private string aula_name = "";
    private string aula_last_name = "";
    private string aula_class_group = "";
    private string aula_school_name = "";

    public GameObject sesion_id_curso_bg;
    public GameObject sesion_institucion_bg;

    public GameObject sesion_curso_bg;
    public GameObject sesion_user_bg;

    public Button btnIniciar;
    public GameObject botonesAulaDos;
    public GameObject botonesAulaUno;
    public GameObject botonesAulaUnoOffline;


    //Botones Menu
    public GameObject btnInformacion;
    public Button btnReintentar;
    public Button btnContinuar;
    public Button btnSalir;
    public Button btnSalirAplicacion;

    //Reporte
    [Header("Reporte PDF"), Space(10)]
    public Text reporte_Tiempo;
    public Text reporte_Usuario;
    public Text reporte_Curso;
    public Text reporte_idCurso;
    public Text reporte_Fecha;
    public Text reporte_Situacion;
    public Text reporte_institucion;
    public Text reporte_unidad;
    public Text reporte_intentos;
    public Text reporte_calificacion;

    public Text preguntas;
    public Text preguntasOrientadoras;

    public GameObject hoja;
    public GameObject hoja1Reporte;
    public GameObject hoja2Reporte;

    public Text texto2preguntasOrientadoras;
    public Text textoGrafica;
    public Button btnReporte;
    public Button btnVisualizarPDF;
    public Button btnEnviarCorreo;
    public Text pruebasTablaTextos;
    public Text pruebasTablaDatos;
    public List<GameObject> listaTablaDatos = new List<GameObject>();
    public GameObject refLabel;

    //Scroll
    public Scrollbar scrollBarInformacion;

    //Scripts
    public Simulador simulador;
    public Reporte reporte;
    public Evaluacion evaluacion;

    //Animacion Pantalla Digital y Compartimiento
    private Vector3 posicionPantallaAdentro;
    private Vector3 posicionPantallaAfuera;
    private Vector3 posicionFinalPantalla;
    private Vector3 rotacionInicialCompartiemiento;
    private Vector3 rotacionFinalCompartiemiento;
    private Vector3 EscalaFinalPantalla;
    private Vector3 escalaInicialPantalla;
    private GameObject refPantalla3D;
    private GameObject refCompartimientoMesa3D;

    //Indicadores Generales
    [Header("Datos sesion")]
    public float segundosSesion;
    public Text ref_Tiempo;
    public Text hud_Usuario;
    public Text hud_Intentos;

    //Otros
    private bool practicaLibre = true;
    public bool simulacionTerminada = false;
    public static bool pantallaActiva = false;
    public static bool pantallaBloqueada = false;
    [HideInInspector] public bool ejecutandoEjercicio = false;
    private bool guardadoHoja2Reporte = false;


    public Text textoInstruccinesPantalla;
    public PanelInformacionEjercicio panelInformacionEjercicio;
    //Dependiendo si es practica o no se desabilitan
    public GameObject botonAdelanteReporte;
    public GameObject botonAtrasReporte;
    public GameObject botonTerminarReporte;
    public ControlIdiomas controlIdiomas;


    //variables de seguridad
    public string licencia_url;
    public GameObject botonesValidacion;

    //estado del idioma
    public string IdiomaActual;

    private float timeOut = 1;

    private string TextoPracticaLibre = "Práctica libre";
    private string TextoReto = "Reto";
    public GameObject botonAceptarCerrar;
    
    /// <summary>
    /// /// Seguridad nueva datos
    /// /// </summary>
    private string[] cmd;
    private string correoMono;
    private string nombreMono;
    private string instituMono;
    public clscontrolBarraBotones clsBarBotones;

    //public UILabel pruebaUTF;
    IEnumerator Start()
    {
        ControlIdiomas Ctrl_Idi_Ref = FindObjectOfType<ControlIdiomas>();
        IdiomaActual = (Ctrl_Idi_Ref.idioma).ToString();
        Debug.Log(IdiomaActual.ToString() + "LEO");
        Debug.Log("Running Version 09-06-2021 7pm");
        checkOptionLang();

        simulador.control.SetActiveFirstPersonController(false);

        DesactivaPanelesPantallaDigital();
        pantallaActiva = false;

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            AbrirPanel(btnSalirAplicacion.gameObject);

        yield return null;

        float[] tmpArray = new float[0];
        grafico2D.DibujarFermentacion(ref tmpArray, ref tmpArray, ref tmpArray);
        grafico2D.DibujarDestilacion(ref tmpArray, ref tmpArray, ref tmpArray);
        grafico2D.DibujarAminoacido(ref tmpArray, ref tmpArray);
        BtnSeleccionarAtras();

        ChangeFormulaText();

		if(simulador.tipoAplicacion == Simulador.TipoAplicacion.WEB && simulador.ModoLTI == true){

            sesion_usuario.interactable=false;
            sesion_IdCurso.interactable = false;
            sesion_curso.interactable = false;
            sesion_Institucion.interactable = false;

            string[] _parametrosLTI = Application.absoluteURL.Split ('?');

			if (_parametrosLTI.Length > 1) {
				string[] arrayParametros = _parametrosLTI[1].Split('&');

				simulador.usuario = WWW.UnEscapeURL(arrayParametros[0]);
				simulador.id_curso = WWW.UnEscapeURL(arrayParametros[1]);
				simulador.curso = WWW.UnEscapeURL(arrayParametros [2]);
				simulador.institucion = WWW.UnEscapeURL(arrayParametros[3]);
				simulador.parametros_lti = WWW.UnEscapeURL(arrayParametros[4]);

				GameObject.FindObjectOfType<ControlIdiomas> ().MtdtradusirInputs ();

			}

		}
    }

    public void ChangeFormulaText()
    {
            switch (IdiomaActual)
            {
                case "Ingles":
                    textoFormula.text = "          alcohol                    V alcohol     \n% ------------------- = ----------------------  \n    liquido a destilar      V liquido a destilar";
                break;
                case "Espanol":
                    textoFormula.text = "          alcohol                    V alcohol     \n% ------------------- = ----------------------  \n    liquido a destilar      V liquido a destilar";
                break;
                case "Portugues":
                    textoFormula.text = "          alcohol                    V alcohol     \n% ------------------- = ----------------------  \n    liquido a destilar      V liquido a destilar";
                break;

                case "Turco":
                textoFormula.text = "              alkol                       V alkol      \n% ------------------- = ----------------------  \n    damıtılacak sıvı        V damıtılacak sıvı";
                break;
            }

        
    }



    public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
    {

        if (args == null) args = new object[] { null };
        IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
        jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);
        try
        {
            IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
            if (IntPtr.Zero != returnValue)
            {
                var val = AndroidJNI.GetStringUTFChars(returnValue);
                AndroidJNI.DeleteLocalRef(returnValue);
                return val;
            }
        }
        finally
        {
            AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
        }

        return null;

    }

    public void mtdBtnAceptarCerrar(){

        Application.Quit();
    }

    public void checkOptionLang()
    {
        FindObjectOfType<Simulador>().CargarExternalXML();

        if (SelLangOption == true)
        {
            simulador.control.SetActiveFirstPersonController(false);

            if (pairLanguages == "ENG-SPA")
            {
                AbrirPanel(PanelSelIdioma);
                FindObjectOfType<Reporte>()._checkLanguage = true;
                SelLangOption = false;
            }

            if (pairLanguages == "ENG-POR")
            {
                AbrirPanel(PanelSelLanguage_ENG_POR);
                FindObjectOfType<Reporte>()._checkLanguage = true;
                SelLangOption = false;
            }
            //checkSeguridadOption();
        }
        else
        {
            AbrirPanel(PanelSelIdioma, false);
            AbrirPanel(PanelSelLanguage_ENG_POR, false);
            FindObjectOfType<Reporte>()._checkLanguage = false;
            checkSeguridadOption();
        }
    }

    public void cambiarLangtoEng()
    {
        ControlIdiomas idioma = FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Ingles;

        FindObjectOfType<ControlIdiomas>().cambio_spa_to_eng_labels();

        SimuladorGenerico[] simuladores = FindObjectsOfType<SimuladorGenerico>();
        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }

        FindObjectOfType<CambiarTexturaxIdioma>().applyTextures();

        FindObjectOfType<ControlIdiomas>().TraducirTodosLabels();

        //GameObject.FindObjectOfType<ControlIdiomas> ().CambiarTexturaIngles_R ();

        IdiomaActual = (idioma.idioma).ToString();
        idioma.MtdtradusirInputs();

        FindObjectOfType<Reporte>().CambiarSimbolosPreguntas();

        checkOptionLang();
    }

    public void cambiarLangtoSpa()
    {
        ControlIdiomas idioma = FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Espanol;

        SimuladorGenerico[] simuladores = FindObjectsOfType<SimuladorGenerico>();
        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }

        FindObjectOfType<ControlIdiomas>().TraducirTodosLabels();

        //GameObject.FindObjectOfType<ControlIdiomas> ().CambiarTexturaEspanol_R ();

        IdiomaActual = (idioma.idioma).ToString();

        FindObjectOfType<Reporte>().CambiarSimbolosPreguntas();
        idioma.MtdtradusirInputs();
        checkOptionLang();
    }

    public void cambiarLangtoPortuguese()
    {
        //tomar variable
        ControlIdiomas idioma = FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Portugues;

        //cambio de labels
        FindObjectOfType<ControlIdiomas>().cambio_spa_to_portugues_labels();

        //cambio de los textos
        SimuladorGenerico[] simuladores = FindObjectsOfType<SimuladorGenerico>();
        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }

        //aplicar texturas -- carga de ecuaciones
        FindObjectOfType<CambiarTexturaxIdioma>().applyTextures();//carga de ecuaciones

        //aplicar traducción a los labels
        FindObjectOfType<ControlIdiomas>().TraducirTodosLabels();
        //GameObject.FindObjectOfType<ControlIdiomas> ().TraductorTextMesh ();
        //GameObject.FindObjectOfType<ControlIdiomas> ().CambiarTexturaPortugues_R ();
        Debug.Log("Laaaaang  " + IdiomaActual);
        //---
        //Debug.Log (idioma.idioma + "OBJECT");
        IdiomaActual = (idioma.idioma).ToString();
        Debug.Log("After  " + IdiomaActual);

        FindObjectOfType<Reporte>().CambiarSimbolosPreguntas();
        idioma.MtdtradusirInputs();
        checkOptionLang();

    }

    public void cambiarLangtoTurco()
    {
        ControlIdiomas idioma = FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Turco;

        FindObjectOfType<ControlIdiomas>().cambio_spa_to_turco_labels();

        SimuladorGenerico[] simuladores = FindObjectsOfType<SimuladorGenerico>();
        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }

        FindObjectOfType<CambiarTexturaxIdioma>().applyTextures();

        FindObjectOfType<ControlIdiomas>().TraducirTodosLabels();

        //GameObject.FindObjectOfType<ControlIdiomas> ().CambiarTexturaIngles_R ();

        IdiomaActual = (idioma.idioma).ToString();
        idioma.MtdtradusirInputs();

        FindObjectOfType<Reporte>().CambiarSimbolosPreguntas();

        checkOptionLang();
    }

    void checkSeguridadOption()
    {
        var_seguridad = FindObjectOfType<Simulador>().SeguridadSwitch;
        var_aula = FindObjectOfType<Simulador>().ModoAula;
        sesion_mensaje_error.enabled = false;

        if (var_seguridad)
        {

            //
            //OLD - getLicenseData();
#if !UNITY_WEBGL
            if (Validacion.Validar())
            {

                //check - new mono
                if (simulador.ModoMonoUsuario)
                {
                    check_new_mono();
                }

                //simulador.mono_user_name = "hola";

                //TODO: Security module
                botonesValidacion.SetActive(false);
                AbrirPanel(panelInicioSesion.gameObject);
                Debug.Log("eureka");
                if (var_aula)
                {
                    activarModoAula();
                }
            }
            else
            {
                //OLD - loadAlertClose();
                //Alerta de advertencia

                AbrirPanel(panelMensajes);
                botonAceptarCerrar.SetActive(true);
                switch (IdiomaActual)
                {

                    case "Ingles":
                        textoMensaje.text = controlIdiomas.Traductor("It was not possible to verify your license number. Please make sure you are opening the application from the CloudLabs main menu.");
                        break;

                    case "Espanol":
                        textoMensaje.text = controlIdiomas.Traductor("No ha sido posible comprobar su número de licencia. Asegúrese que esté abriendo esta aplicación desde el menú principal CloudLabs.");
                        break;

                    case "Portugues":
                        textoMensaje.text = controlIdiomas.Traductor("Não foi possível validar seu número de licença. Assegure-se se está abrindo este aplicativo a partir do menu principal CloudLabs.");
                        break;

                    case "Turco":
                        textoMensaje.text = controlIdiomas.Traductor("Lisans numaranızı doğrulamak mümkün değildi. Lütfen uygulamayı CloudLabs ana menüsünden açtığınızdan emin olun.");
                        break;
                }
            }
#endif

        }
        else
        {
            botonesValidacion.SetActive(false);
            AbrirPanel(panelInicioSesion);

            if (var_aula)
            {
                activarModoAula();
            }
        }
    }

    void check_new_mono()
    {
        //NEW MONO
        string[] _array_invoke;

#if UNITY_ANDROID
        //using (AndroidJavaClass jc = new AndroidJavaClass("com.cloudlabs.triangulos")){
        //  cmdInfo = jc.CallStatic<string>("getLauncherURL");
        //}
        //if (cmdInfo != "no input data" && cmdInfo != ""){
        if (simulador.ModoMonoUsuario == true)
        {
            //Seguridad 2
            try
            {
                AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                if (currentActivity != null)
                {
                    AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
                    if (intent != null)
                    {

                        simulador.mono_user_name = safeCallStringMethod(intent, "getStringExtra", "nombre");
                        simulador.mono_user_institution = safeCallStringMethod(intent, "getStringExtra", "institucion");
                        simulador.mono_user_email = safeCallStringMethod(intent, "getStringExtra", "correo");


                        sesion_usuario.text = safeCallStringMethod(intent, "getStringExtra", "nombre");
                        sesion_Institucion.text = safeCallStringMethod(intent, "getStringExtra", "institucion");


                    }
                }

            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }

#elif (UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_IOS)
        string[] args = Environment.GetCommandLineArgs();
        //if (args.Length > 1)
        //{

        if (simulador.ModoMonoUsuario == true)
        {
            _array_invoke = args[1].Split(',');
            //license_number = _array_invoke[0];

            //Nueva seguridad traida de datos
            cmd = System.Environment.CommandLine.Split(',');

            

            try
            {
                if (cmd.Length > 5)
                { 

                    //Datos del LTI
                    simulador.mono_user_name = cmd[3];
                    simulador.mono_user_institution = cmd[4];
                    simulador.mono_user_email = cmd[5];

                    sesion_usuario.text = cmd[3];
                    sesion_Institucion.text = cmd[4];

                }
            }
            catch
            {

            }
        }
        else
        {
            //license_number = args[1];
            
        }
        // -- END NEW MONO
#endif
    }

    void getLicenseData()
    {

        string bundle_id = "com.genmedia.quimicaesp";
        //string device_id = SystemInfo.deviceUniqueIdentifier;
        string device_id = "";
        string license_number = "";//53392-F5BD8-F445C-EB286

        string cmdInfo = "";
		string[] _array_invoke;

        if (Application.platform == RuntimePlatform.Android)
        {
#if UNITY_ANDROID
			using (AndroidJavaClass jc = new AndroidJavaClass("com.genmedia.quimicaesp.MainActivity")){
				cmdInfo = jc.CallStatic<string>("getLauncherURL");
				device_id = jc.CallStatic<string>("getDeviceData");
			}
#endif
            if (cmdInfo != "no input data" && cmdInfo != "")
            {
				if (simulador.ModoMonoUsuario == true) {
					_array_invoke = cmdInfo.Split (',');
					license_number = _array_invoke[0].Substring(_array_invoke[0].IndexOf("LICENCIA=") + 9);
					bundle_id = _array_invoke[0].Substring(0, _array_invoke[0].IndexOf("://"));
					try {
						simulador.mono_user_name = _array_invoke[1];
						simulador.mono_user_institution = _array_invoke[2];
						simulador.mono_user_email = _array_invoke[3];

						GameObject.FindObjectOfType<ControlIdiomas> ().MtdtradusirInputs ();
					}
					catch(Exception e){}
				} else {
					license_number = cmdInfo.Substring(cmdInfo.IndexOf("LICENCIA=") + 9);
					bundle_id = cmdInfo.Substring(0, cmdInfo.IndexOf("://"));
				}
            }
            else
            {
                license_number = "";
            }
        }
        else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
			Debug.Log("estoy en seguridad 1");

#if !UNITY_WEBPLAYER

            List<string> macsArray = new List<string>();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in nics)
            {
                PhysicalAddress address = adapter.GetPhysicalAddress();
                byte[] bytes = address.GetAddressBytes();
                string mac = null;
                for (int i = 0; i < bytes.Length; i++)
                {
                    mac = string.Concat(mac + (string.Format("{0}", bytes[i].ToString("X2"))));
                    if (i != bytes.Length - 1)
                    {
                        mac = string.Concat(mac + "-");
                    }
                }
                if (mac != null)
                {
                    macsArray.Add(mac);
                }
                //Debug.Log(mac);

                device_id = string.Join(",", macsArray.ToArray());

                string[] args = Environment.GetCommandLineArgs();
                //license_number = string.Join(",", args);
                //Debug.Log(args);

                if (args.Length > 1)
                {
					if (simulador.ModoMonoUsuario == true) {
						_array_invoke = args[1].Split (',');
						license_number = _array_invoke[0];
						try {
							simulador.mono_user_name = _array_invoke[1];
							simulador.mono_user_institution = _array_invoke[2];
							simulador.mono_user_email = _array_invoke[3];

							GameObject.FindObjectOfType<ControlIdiomas> ().MtdtradusirInputs ();
						}
						catch(Exception e){}
					}
					else {
						license_number = args[1];
					}
                }
				else {
					
					/*
					if (simulador.ModoMonoUsuario == true) {
						_array_invoke = "44D24-E5F70-398E2-75636,Edgar Forero,Toro Labs,edgar@toro-labs.com".Split (',');

						license_number = _array_invoke[0];
						try {
							simulador.mono_user_name = _array_invoke[1];
							simulador.mono_user_institution = _array_invoke[2];
							simulador.mono_user_email = _array_invoke[3];

							GameObject.FindObjectOfType<ControlIdiomas> ().MtdtradusirInputs ();
						}
						catch(Exception e){}

					}
					else {
						license_number = "";
					}
					*/

                }
            }
#endif
        }

		if (simulador.tipoAplicacion == Simulador.TipoAplicacion.WEB && simulador.ModoLTI == true) {
			if (!Application.absoluteURL.Contains (simulador.valid_lti_url)) {
				AbrirPanel (panelMensajes.gameObject);
				switch (IdiomaActual) {

				case "Ingles":
					textoMensaje.text = controlIdiomas.Traductor ("The application was opened via an invalid path. Please contact your provider.");
					break;

				case "Espanol":
					textoMensaje.text = controlIdiomas.Traductor ("La aplicación se abrió desde una ruta inválida. Asegúrese que se abra desde el curso respectivo.");
					break;

				case "Portugues":
					textoMensaje.text = controlIdiomas.Traductor ("O aplicativo foi aberto a partir de um caminho inválido. Assegure-se que ele seja aberto a partir de seu respectivo curso.");
					break;

                case "Turco":
                    textoMensaje.text = controlIdiomas.Traductor ("Uygulama geçersiz bir yolla açıldı. Lütfen sağlayıcınıza başvurun.");
                    break;
				}
			} else {
				botonesValidacion.SetActive (false);
				AbrirPanel (panelInicioSesion.gameObject);
				Debug.Log ("eureka");
				if (var_aula) {
					activarModoAula ();
				}
			}
		} else {
			//license_number = "44D24-E5F70-398E2-75636"; // para pruebas

			if (license_number != "")
			{

				if (PlayerPrefs.HasKey("attempts"))
				{
					licencia_url = "http://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(license_number) + "&primera_vez=false";
				}
				else
				{
					licencia_url = "http://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(license_number) + "&primera_vez=true";
				}

				WWW www = new WWW(licencia_url);
				StartCoroutine(WaitForRequest(www));
				//Debug.Log (url);
			}
			else
			{
				AbrirPanel(panelMensajes);

                botonAceptarCerrar.SetActive(true);

                if (IdiomaActual == "Ingles")
				{

				}
				else if (IdiomaActual == "Espanol")
				{

				}

				switch (IdiomaActual)
				{

				case "Ingles":
					textoMensaje.text = controlIdiomas.Traductor("It was not possible to verify your license number. Please make sure you are opening the application from the CloudLabs main menu.");
					break;

				case "Espanol":
					textoMensaje.text = controlIdiomas.Traductor("No ha sido posible comprobar su número de licencia. Asegúrese que esté abriendo esta aplicación desde el menú principal CloudLabs.");
					break;

				case "Portugues":
					textoMensaje.text = controlIdiomas.Traductor("Não foi possível validar seu número de licença. Assegure-se se está abrindo este aplicativo a partir do menu principal CloudLabs.");
					break;

                case "Turco":
                    textoMensaje.text = controlIdiomas.Traductor("Lisans numaranızı doğrulamak mümkün değildi. Lütfen uygulamayı CloudLabs ana menüsünden açtığınızdan emin olun.");
                    break;
				}
			}
		}

    }

    IEnumerator WaitForRequest(WWW www)
    {

        Debug.Log("in waitforrequest");
        float timer = 0;
        bool failed = false;

        while (!www.isDone)
        {
            if (timer > timeOut)
            { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }

        if (failed)
        {
            www.Dispose();

            if (PlayerPrefs.HasKey("attempts"))
            {
                int attempts = PlayerPrefs.GetInt("attempts");
                if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                {
                    AbrirPanel(panelInicioSesion.gameObject);
                    attempts = attempts + 1;
                    PlayerPrefs.SetInt("attempts", attempts);
                    PlayerPrefs.Save();
                    if (var_aula)
                    {
                        activarModoAula();
                    }
                }
                else
                {
                    AbrirPanel(panelMensajes.gameObject);
                    botonesValidacion.SetActive(true);

                    switch (IdiomaActual)
                    {
                        case "Ingles":
                            textoMensaje.text = controlIdiomas.Traductor("It seems too much time has passed since the last time your license was verified. Please connect your device to the internet and click VALIDATE or CANCEL to close the application.");
                            break;
                        case "Espanol":
                            textoMensaje.text = controlIdiomas.Traductor("Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación.");
                            break;
                        case "Portugues":
                            textoMensaje.text = controlIdiomas.Traductor("Parece que passou muito tempo desde a última vez que sua licença foi verificada. Por favor, conecte seu dispositivo à Internet e clique no botão VALIDAR ou CANCELAR para fechar o aplicativo.");
                            break;
                        case "Turco":
                            textoMensaje.text = controlIdiomas.Traductor("Lisansınızın son doğrulanmasından bu yana çok fazla zaman geçmiş gibi görünüyor. Lütfen cihazınızı Internet'e bağlayın ve uygulamayı kapatmak için ONAYLA veya İPTAL Et'i tıklatın.");
                            break;
                    }
                }
            }
            else
            {
                AbrirPanel(panelMensajes.gameObject);
                botonesValidacion.SetActive(true);

                switch (IdiomaActual)
                {
                    case "Ingles":
                        textoMensaje.text = controlIdiomas.Traductor("Oops, it seems there was a problem with the internet connection or the CloudLabs servers. Click VALIDATE to try again or CANCEL to close the application.");
                        break;
                    case "Espanol":
                        textoMensaje.text = controlIdiomas.Traductor("Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación.");
                        break;
                    case "Portugues":
                        textoMensaje.text = controlIdiomas.Traductor("Parece que algo deu errado na conexão com a Internet ou com os servidores CloudLabs. Clique no botão VALIDAR para tentar outra vez ou CANCELAR para fechar o aplicativo.");
                        break;
                    case "Turco":
                        textoMensaje.text = controlIdiomas.Traductor("İnternet bağlantısı veya CloudLabs sunucularıyla ilgili bir sorun var gibi görünüyor. Tekrar denemek için DOĞRULA'yı, uygulamayı kapatmak için İPTAL'i tıklatın.");
                        break;
                }
            }

        }
        else
        {

            if (www.error == null)
            {
                Debug.Log("WWW Ok!: " + www.text);
                var _data = JSON.Parse(www.text);
                switch (_data["result"].Value)
                {
                    case "error":
                        switch (_data["message_id"].AsInt)
                        {
                            case 1:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("The license number you have used to activate the product is invalid. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("El número de licencia con el que se ha activado el producto es inválido. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("O número de licença com o qual você ativou o produto é inválido. Por favor, entre em contato com o seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Ürünü etkinleştirmek için kullandığınız lisans numarası geçersiz. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 2:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("Your license has not been activated yet. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Su licencia aún no ha sido activada. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Sua licença ainda não foi ativada. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınız henüz etkinleştirilmedi. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 3:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("Your license has been deactivated. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Su licencia ha sido desactivada. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Sua licença foi desativada. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınız devre dışı bırakılmış. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 4:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("Your license has expired. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Su licencia ha caducado. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Sua licença expirou. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınızın süresi dolmuş. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 5:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("Your license does not allow you to execute this application. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("La licencia adquirida no permite ejecutar esta aplicación. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("A licença adquirida não permite executar este aplicativo. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınız bu uygulamayı yürütmenize izin vermiyor. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 6:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("You have exceeded the number of activations allowed for your license. Please contact your supplier.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Se ha excedido el número de activaciones permitidas por su licencia. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Foi excedido o número de atividades permitidas para sua licença. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınız için izin verilen etkinleştirme sayısını aştınız. Lütfen tedarikçinizle iletişime geçin.");
                                        break;
                                }
                                break;
                            case 7:
                                if (PlayerPrefs.HasKey("attempts"))
                                {
                                    PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                    PlayerPrefs.Save();
                                }
                                AbrirPanel(panelMensajes.gameObject);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("The validation process was unsuccessful. Please reinstall the application and try again.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("El proceso de validación arrojó un resultado inválido. Por favor reinstale la aplicación e intente nuevamente.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("O processo de validação produziu um resultado inválido. Por favor, reinstale o aplicativo e tente outra vez.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Doğrulama işlemi başarısız oldu. Lütfen uygulamayı yeniden kurun ve yeniden deneyin.");
                                        break;
                                }
                                break;
                        }
                        break;
                    case "success":
                        PlayerPrefs.SetInt("attempts", 0);
                        PlayerPrefs.SetInt("offline_attempts", _data["offline_attempts"].AsInt);
                        PlayerPrefs.Save();
                        AbrirPanel(panelInicioSesion.gameObject);
                        if (var_aula)
                        {
                            activarModoAula();
                        }
                        break;
                }
            }
            else
            {
                if (PlayerPrefs.HasKey("attempts"))
                {
                    int attempts = PlayerPrefs.GetInt("attempts");
                    if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                    {
                        AbrirPanel(panelInicioSesion.gameObject);
                        attempts = attempts + 1;
                        PlayerPrefs.SetInt("attempts", attempts);
                        PlayerPrefs.Save();
                        if (var_aula)
                        {
                            activarModoAula();
                        }
                    }
                    else
                    {
                        AbrirPanel(panelMensajes.gameObject);
                        botonesValidacion.SetActive(true);

                        switch (IdiomaActual)
                        {
                            case "Ingles":
                                textoMensaje.text = controlIdiomas.Traductor("It seems too much time has passed since the last time your license was verified. Please connect your device to the internet and click VALIDATE or CANCEL to close the application.");
                                break;
                            case "Espanol":
                                textoMensaje.text = controlIdiomas.Traductor("Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación.");
                                break;
                            case "Portugues":
                                textoMensaje.text = controlIdiomas.Traductor("Parece que passou muito tempo desde a última vez que sua licença foi verificada. Por favor, conecte seu dispositivo à Internet e clique no botão VALIDAR ou CANCELAR para fechar o aplicativo.");
                                break;
                            case "Turco":
                                textoMensaje.text = controlIdiomas.Traductor("Lisansınızın son doğrulanmasından bu yana çok fazla zaman geçmiş gibi görünüyor. Lütfen cihazınızı Internet'e bağlayın ve uygulamayı kapatmak için ONAYLA veya İPTAL Et'i tıklatın.");
                                break;
                        }
                    }
                }
                else
                {
                    AbrirPanel(panelMensajes.gameObject);
                    botonesValidacion.SetActive(true);

                    switch (IdiomaActual)
                    {
                        case "Ingles":
                            textoMensaje.text = controlIdiomas.Traductor("Oops, it seems there was a problem with the internet connection or the CloudLabs servers. Click VALIDATE to try again or CANCEL to close the application.");
                            break;
                        case "Espanol":
                            textoMensaje.text = controlIdiomas.Traductor("Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación.");
                            break;
                        case "Portugues":
                            textoMensaje.text = controlIdiomas.Traductor("Parece que algo deu errado na conexão com a Internet ou com os servidores CloudLabs. Clique no botão VALIDAR para tentar outra vez ou CANCELAR para fechar o aplicativo.");
                            break;
                        case "Turco":
                            textoMensaje.text = controlIdiomas.Traductor("İnternet bağlantısı veya CloudLabs sunucularıyla ilgili bir sorun var gibi görünüyor. Tekrar denemek için DOĞRULA'yı, uygulamayı kapatmak için İPTAL'i tıklatın.");
                            break;
                    }
                }
            }


        }

    }

    public void cancelarLicencia()
    {
        Application.Quit();
    }

    public void validarLicencia()
    {
        Debug.Log("Estado seguridad " + var_seguridad);

        botonesValidacion.SetActive(false);
        AbrirPanel(panelMensajes.gameObject, false);
        WWW www = new WWW(licencia_url);
        StartCoroutine(WaitForRequest(www));
    }

    void DesactivaPanelesPantallaDigital()
    {
        panelGraficoDestilacion.SetActive(false);
        panelGraficoFermentacion.SetActive(false);
        panelGraficoAminoacido.SetActive(false);
        panelCalculadora.SetActive(false);
        panelEcuaciones.SetActive(false);
        panelInstrucciones.SetActive(false);
    }

    private void activarModoAula()
    {
        
        sesion_institucion_bg.SetActive(false);
        sesion_Institucion_label.SetActive(false);
        sesion_id_curso_bg.SetActive(false);
        sesion_IdCurso_label.SetActive(false);  

        

        sesion_curso.inputType = InputField.InputType.Password;

		if (simulador.ModoMonoUsuario == true) {

            sesion_usuario.text = simulador.mono_user_email;
            sesion_curso.text = simulador.mono_user_email;

            sesion_usuario.readOnly = true;
            sesion_curso.readOnly = true;

            //TODO
            sesion_IdCurso.text = "-Idcurso";
            sesion_Institucion.text = "-Institucion";
           

		} else {
			sesion_usuario.text = "";
			sesion_curso.text = "";
            /// llamar a al calse webAulaa
#if UNITY_WEBGL
        if (!FindObjectOfType<Simulador>().ModoLTI)
            claseWebAula.ObtenerDatosWEbAula();
#endif
        }

        switch (IdiomaActual)
        {
            case "Ingles":
                sesion_curso_label.text = "Password";
                break;
            case "Espanol":
                sesion_curso_label.text = "Contraseña";
                break;
            case "Portugues":
                sesion_curso_label.text = "Senha";
                break;
            case "Turco":
                sesion_curso_label.text = "Parola";
                break;
        }
    }

    public void Grafico()
    {
        if (!panelGraficoDestilacion.activeSelf || !panelGraficoFermentacion.activeSelf || !panelGraficoAminoacido.activeSelf)
        {
            ActivarGrafico(true);
            panelCalculadora.SetActive(false);
            panelEcuaciones.SetActive(false);
            panelInstrucciones.SetActive(false);
        }
    }

    public void Calculadora()
    {
        if (!panelCalculadora.activeSelf)
        {
            panelCalculadora.SetActive(true);
            ActivarGrafico(false, true);
            panelEcuaciones.SetActive(false);
            panelInstrucciones.SetActive(false);
        }
    }

    public void Ecuaciones()
    {
        if (!panelEcuaciones.activeSelf)
        {
            panelEcuaciones.SetActive(true);
            ActivarGrafico(false, true);
            panelCalculadora.SetActive(false);
            panelInstrucciones.SetActive(false);
        }
    }

    public void Instrucciones()
    {
        if (!panelInstrucciones.activeSelf)
        {
            panelInstrucciones.SetActive(true);
            ActivarGrafico(false, true);
            panelCalculadora.SetActive(false);
            panelEcuaciones.SetActive(false);
        }
    }

    public void Reporte()
    {
        //		panelCalculadora.SetActive (false);
        //		panelEcuaciones.SetActive (false);
        //		panelInstrucciones.SetActive (false);
        //		ActivarGrafico (true);	
        //		Simulador.simulacionIniciada = false;
        //		StartCoroutine(GuardarGraficas());

        if (practicaLibre)
        {
            //Verificar que ya exista alemenos 1 intento
            if (simulador.simuladorActivo.ejecutoVariosEjercicios)
            {
                ActivarGrafico(true);
                panelCalculadora.SetActive(false);
                panelEcuaciones.SetActive(false);
                panelInstrucciones.SetActive(false);
                StartCoroutine(GuardarGraficas());
                simulador.simuladorActivo.ActualizarDatosReporte();
                simulacionTerminada = true;
            }
        }
        else
        {
            //Verificar que realizo el ejercicio correctamente
            if (simulacionTerminada)
            {
                panelCalculadora.SetActive(false);
                panelEcuaciones.SetActive(false);
                panelInstrucciones.SetActive(false);
                ActivarGrafico(true);
                StartCoroutine(GuardarGraficas());
            }
        }
    }

    private void ActivarGrafico(bool active, bool desactivarTodos = false)
    {
        if (desactivarTodos)
        {
            panelGraficoFermentacion.SetActive(active);
            panelGraficoDestilacion.SetActive(active);
            panelGraficoAminoacido.SetActive(active);
            return;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
        {
            panelGraficoFermentacion.SetActive(active);
            panelGraficoDestilacion.SetActive(!active);
            panelGraficoAminoacido.SetActive(!active);
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
        {
            panelGraficoDestilacion.SetActive(active);
            panelGraficoFermentacion.SetActive(!active);
            panelGraficoAminoacido.SetActive(!active);
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorAminoAcido))
        {
            panelGraficoAminoacido.SetActive(active);
            panelGraficoDestilacion.SetActive(!active);
            panelGraficoFermentacion.SetActive(!active);
        }
    }

    private void CerrarPantallaPaneles()
    {
        DesactivarPantalla();
        AbrirPanel(panelPantalla.gameObject, false);
        panelEjercicios.SetActive(false);
        panelMensajes.gameObject.SetActive(false);
    }

    IEnumerator GuardarGraficas()
    {
        btnReporte.gameObject.SetActive(false);

        RawImage uiTextureActivo = null;

        if (simulador.simuladorActivo.GetType() == typeof(SimuladorAminoAcido))
        {
            uiTextureActivo = grafico2D.uiTextureGraficoAminoacido;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
        {
            uiTextureActivo = grafico2D.uiTextureGraficoDestilacion;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
        {
            uiTextureActivo = grafico2D.uiTextureGraficoFermentacion;
        }

        yield return new WaitForEndOfFrame();

        // Leer los pixeles del grafico en la textura
        Texture2D tex = CapturaPantalla.CapturarImagenRectTransform(uiTextureActivo.rectTransform);

        // Incializar las graficas del reporte. 
        reporte.graficasReporte[0].texture = tex; 

        float anteriorTIme = Time.timeScale;
        Time.timeScale = 1;
        yield return new WaitForSeconds(1);
        Time.timeScale = anteriorTIme;

        btnReporte.gameObject.SetActive(true);

        //Se va a dejar igual al de sim. de fisica  : modificado el 9 feb .
        CerrarPantallaPaneles();
    }

    public void Cerrar()
    {
        if (!simulacionTerminada)
        {
            AbrirPanel(panelPantalla.gameObject, false);
            AbrirPanel(panelEntradaDatos.gameObject);
            AbrirPanel(btnInformacion);
            AbrirPanel(panelIntentosReloj.gameObject);
            AbrirPanel(btnSalir.gameObject);
            DesactivarPantalla();
        }
    }

    public void AccionarBtnPantalla()
    {
        if (!pantallaActiva)
        {
            refPantalla3D.SetActive(true);
            AbrirPantalla();
        }
    }

    public void AbrirPantalla()
    {
        if (!pantallaActiva)
        {
            pantallaActiva = true;
            DesplazarPantalla();
        }
    }

    //Menus
    public IEnumerator MostrarTextos(float tiempo, SimuladorGenerico simulador)
    {
        yield return new WaitForSeconds(0);
        DesactivaPanelesPantallaDigital();
        simulacionTerminada = false;
        ejecutandoEjercicio = false;
        AsignarDatosPantalla3D();
        AsignarTextosPantalla(simulador);
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            AbrirPanel(btnSalirAplicacion.gameObject, false);
    }

    public void AsignarDatosPantalla3D()
    {
        refPantalla3D = simulador.simuladorActivo.pantalla3D;
        refCompartimientoMesa3D = simulador.simuladorActivo.compartimientoMesa3D;
        posicionPantallaAfuera = refPantalla3D.transform.position + new Vector3(0, 0.16f, 0);
        posicionPantallaAdentro = refPantalla3D.transform.position;
        escalaInicialPantalla = refPantalla3D.transform.localScale;
        rotacionInicialCompartiemiento = refCompartimientoMesa3D.transform.localRotation.eulerAngles;
        rotacionFinalCompartiemiento = refCompartimientoMesa3D.transform.localRotation.eulerAngles + new Vector3(90, 0, 0);
    }

    public void AsignarTextosPantalla(SimuladorGenerico simulador)
    {
        AbrirPanel(panelInicial.gameObject);
        scrollBarInformacion.value = 0;
        //AbrirPanel(panelHud.gameObject); hadron
        AbrirPanel(panelEjercicios, false);
        AbrirPanel(panelMensajes.gameObject, false);
        textoInstruccinesPantalla.text = simulador.textoInstruccionesPantalla;
        titulo.text = simulador.titulo;
        simulador.entradaDatos.SetActive(true);
        Invoke("Abrir", 2);
    }

    void Abrir()
    {
        BtnSeleccionarModoParabolico();
        clsBarBotones.ActivarBarraMayor(true);
        //AbrirPanel(panelPortada.gameObject);
    }

    void AsignarTextoSimulacion(string texto)
    {
        textoSimulacion.text = texto;
        textoSimulacion.transform.parent.parent.GetComponent<ScrollRect>().normalizedPosition = Vector2.zero;
    }

    public void BtnSeleccionarModoParabolico()
    {
        // nota mejorar esta condicion 
        if (labelRetoPractica.text == TextoPracticaLibre)
        {
            panelPortada.SetActive(false);
            panelEjercicios.SetActive(true);
            AsignarTextoSimulacion(simulador.simuladorActivo.textoMisionPracticaLibre);
            practicaLibre = true;
        }
        else
        {
            panelPortada.SetActive(false);
            panelEjercicios.SetActive(true);
            AsignarTextoSimulacion(simulador.simuladorActivo.textoMisionReto);
            practicaLibre = false;
        }
    }

    public void BtnComoJugar()
    {
        AsignarTextoSimulacion((practicaLibre) ? simulador.simuladorActivo.textoComoJugarPracticaLibre : simulador.simuladorActivo.textoComoJugarReto);
        scrollBarInformacion.value = 0;
    }

    public void BtnMision()
    {
        AsignarTextoSimulacion((practicaLibre) ? simulador.simuladorActivo.textoMisionPracticaLibre : simulador.simuladorActivo.textoMisionReto);
        scrollBarInformacion.value = 0;
    }

    public void BtnInformacion()
    {
        panelEntradaDatos.gameObject.SetActive(false);
        panelInformacionEjercicio.ClicBtnInfo();
        AbrirPanel(panelIntentosReloj.gameObject, false);
        AbrirPanel(btnInformacion, false);
        AbrirPanel(btnSalir.gameObject, false);
        BtnMisionInformacion();
    }

    public void BtnCerrarInformacion()
    {
        AbrirPanel(panelEntradaDatos.gameObject);
        AbrirPanel(panelIntentosReloj.gameObject);
        AbrirPanel(btnSalir.gameObject);
        AbrirPanel(btnInformacion);
    }

    public void BtnMisionInformacion()
    {
        AsignarTextoSimulacion(practicaLibre ? simulador.simuladorActivo.textoMisionPracticaLibre : simulador.simuladorActivo.textoMisionReto);
        scrollBarInformacion.value = 0;
    }

    public void BtnComoJugarInformacion()
    {
        AsignarTextoSimulacion(practicaLibre ? simulador.simuladorActivo.textoComoJugarPracticaLibre : simulador.simuladorActivo.textoComoJugarReto);
        scrollBarInformacion.value = 0;
    }

    public void BtnIniciar()
    {
        AbrirPanel(panelInicial.gameObject, false);
        AbrirPanel(panelEjercicios, false);
        AbrirPanel(panelEntradaDatos.gameObject);
        AbrirPanel(btnInformacion);
        AbrirPanel(panelIntentosReloj.gameObject);
        AbrirPanel(simulador.simuladorActivo.indicadores);
        pantallaActiva = false;
        simulador.simuladorActivo.ReiniciarTiempo();
        simulador.simuladorActivo.BtnIniciar(practicaLibre);
        simulador.simuladorActivo.intentos = 1;
        hud_Intentos.text = "1";
        guardadoHoja2Reporte = false;
        simulador.simuladorActivo.datosPantalla.SetActive(true);
        simulador.simuladorActivo.ecuaciones.SetActive(true);
        AbrirPanel(btnSalir.gameObject);
        Simulador.simulacionIniciada = true;
    }

    public void BtnAtras()
    {
        panelPortada.SetActive(true);
        panelEjercicios.SetActive(false);
    }

    public void BtnReintentar()
    {
        AbrirPanel(btnInformacion, true);
        AbrirPanel(panelMensajes.gameObject, false);
        AbrirPanel(panelInicial.gameObject, false);
        AbrirPanel(panelEntradaDatos.gameObject);
        AbrirPanel(btnInformacion);

        simulador.simuladorActivo.BtnIniciar(practicaLibre);
        ejecutandoEjercicio = false;
        pantallaActiva = false;

        if (!practicaLibre)
            simulador.simuladorActivo.intentos++;
        int _intentos = simulador.simuladorActivo.intentos;
        hud_Intentos.text = _intentos.ToString();
        pantalla_Intentos.text = _intentos.ToString();
        AbrirPanel(simulador.simuladorActivo.indicadores);
        Simulador.ejecutandoEjercicio = false;
    }

    public void BtnSalir()
    {
        simulador.simuladorActivo.usuarioEntroEstacion = false;

        if (!pantallaActiva)
        {
            simulador.simuladorActivo.NoContarTiempo();
            AbrirPanel(btnSalir.gameObject, false);
            //AbrirPanel(panelHud.gameObject, false); hadron
            AbrirPanel(panelEjercicios, false);
            AbrirPanel(panelPortada.gameObject, false);
            AbrirPanel(panelInicial.gameObject, false);
            panelPortada.SetActive(false);
            AbrirPanel(panelPantalla.gameObject, false);
            AbrirPanel(panelEntradaDatos.gameObject, false);
            AbrirPanel(btnInformacion, false);
            AbrirPanel(panelIntentosReloj.gameObject, false);
            AbrirPanel(panelMensajes.gameObject, false);
            simulador.control.SalirCamaraSimulador();
            simulador.simuladorActivo.entradaDatos.SetActive(false);
            simulador.simuladorActivo.indicadores.SetActive(false);
            simulador.simuladorActivo.datosPantalla.SetActive(false);
            simulador.simuladorActivo.ecuaciones.SetActive(false);

            panelEvaluacion.gameObject.SetActive(false);


            Simulador.simulacionIniciada = false;
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
                AbrirPanel(btnSalirAplicacion.gameObject);
        }
        else
            print("cierre Pantalla");

        //Cuando se sale de las simulaciones reiniciar configuraciones de objetos
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
            simulador.simuladorActivo.GetComponent<SimuladorFermentacion>().SalidaFermentacion();
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
            simulador.simuladorActivo.GetComponent<SimuladorDestilacion>().SalidaDestilacion();
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorAminoAcido))
            simulador.simuladorActivo.GetComponent<SimuladorAminoAcido>().SalidaAminoAcido();
    }

    public void BtnSalirAplicacion()
    {
        Application.Quit();
    }

    public void AbrirPanel(GameObject panel, bool abrir = true)
    {
        panel.SetActive(abrir);//coreccion Programador Hadron

        /*//remplazando todo esto
        if (abrir)
        {
            if (!panel.activeInHierarchy)
                panel.SetActive(true);
        }
        else
        {
            if (panel.activeInHierarchy)
                panel.SetActive(false);
        }*/
        //hasta aqui
    }

    public void BtnTerminarSimulacion()
    {
        panelReporte.gameObject.SetActive(false);
        pantallaActiva = false;
        BtnSalir();
        BorrarInstanciasTablaDatos();
    }

    public void BorrarInstanciasTablaDatos()
    {
        foreach (GameObject objeto in listaTablaDatos)
            Destroy(objeto);
        listaTablaDatos.Clear();
    }

    public void BtnContinuar()
    {
        if (practicaLibre)
        {

        }
        else
        {
            if (simulacionTerminada)
            {
                pantallaBloqueada = false;
                AbrirPanel(panelInicial.gameObject, false);
                AbrirPanel(panelEjercicios, false);
                AbrirPanel(panelMensajes.gameObject, false);
                AbrirPanel(panelIntentosReloj.gameObject, false);
                simulador.simuladorActivo.botonPantalla.Click();
                AccionarPantalla();
                Debug.Log("simulacionTerminada");
                return;
            }
            Debug.Log("simulacionTerminada2");

            AbrirPanel(simulador.simuladorActivo.indicadores, false);
            panelInicial.gameObject.SetActive(true);
            panelPortada.SetActive(false);
            panelEjercicios.SetActive(false);
            panelMensajes.gameObject.SetActive(false);
            panelEvaluacion.gameObject.SetActive(true);
            evaluacion.BorrarRespuestas();
            MostrarEvaluacion();
            Debug.Log("simulacionTerminada3");
        }
    }

    private void aulaLoginRequest()
    {
#if UNITY_WEBGL
        string _params = "{\"user\":\"" + sesion_usuario.text + "\",\"pass\":\"" + sesion_curso.text + "\"}";
#else
        string _params = "{\"user\":\"" + sesion_usuario.text + "\",\"pass\":\"" + Md5Sum(sesion_curso.text) + "\"}";
#endif
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(_params);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        string url_get_aula = simulador.url_aula + "/externals/login?data=" + encodedText;
        Debug.Log(url_get_aula);

        WWW wwwAula = new WWW(url_get_aula);
        StartCoroutine(WaitForRequestAula(wwwAula));
        btnIniciar.enabled = false;

        switch (IdiomaActual)
        {
            case "Ingles":
                sesion_mensaje_error.text = "Processing...";
                break;

            case "Espanol":
                sesion_mensaje_error.text = "Procesando...";
                break;

            case "Portugues":
                sesion_mensaje_error.text = "Processando...";
                break;

            case "Turco":
                sesion_mensaje_error.text = "İşleniyor...";
                break;
        }
    }

    public void BtnIniciarSesion()
    {
        print("iniciar sesion ");

        if (var_aula && !var_offline)
        {
            sesion_mensaje_error.enabled = true;

            if (sesion_usuario.text != "" && sesion_curso.text != "")            
                aulaLoginRequest();            
            else
            {
                switch (IdiomaActual)
                {
                    case "Ingles":
                        sesion_mensaje_error.text = "All fields are required.";
                        break;

                    case "Espanol":
                        sesion_mensaje_error.text = "Todos los campos son requeridos.";
                        break;

                    case "Portugues":
                        sesion_mensaje_error.text = "Todos os campos são necessários.";
                        break;

                    case "Turco":
                        sesion_mensaje_error.text = "Tüm alanlar gereklidir.";
                        break;
                }
            }
        }
        else
        {
            if (sesion_usuario.text != "" && sesion_IdCurso.text != "" && sesion_curso.text != "" && sesion_Institucion.text != "")
            {
                simulador.simuladorActivo.reporte.GuardarDatosSesion(sesion_usuario.text, sesion_IdCurso.text, sesion_curso.text);
                hud_Usuario.text = sesion_usuario.text;
                hud_Intentos.text = "0";
                AbrirPanel(panelInicioSesion.gameObject, false);
                simulador.control.SetActiveFirstPersonController(true);
                Datos_Usuario.text = sesion_usuario.text;
            }
            else            
                print("Falta completar los datos del login.");            
        }
    }

    public string Md5Sum(string strToEncrypt)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    IEnumerator WaitForRequestAula(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            byte[] decodedBytes = Convert.FromBase64String(www.text);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);
            Debug.Log(decodedText);
            var _data = JSON.Parse(decodedText);

            if (_data["state"] != null)
            {
                if (_data["state"].Value == "true")
                {
                    if (_data["res_code"] != null)
                    {
                        switch (_data["res_code"].Value)
                        {
                            case "INVALID_USER_PASS":
                                btnIniciar.enabled = true;
                                sesion_mensaje_error.text = "";
                                AbrirPanel(panelMensajes.gameObject);
                                botonesAulaUno.SetActive(true);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("The username and/or password are invalid. Please check and try again.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("El usuario y/o la contraseña son inválidas. Verifíquelas e intente nuevamente.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("O usuário e/ou a senha são inválidos. Verifique e tente outra vez.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Kullanıcı adı ve/veya parola geçersizdir. Lütfen kontrol edip tekrar deneyin.");
                                        break;
                                }
                                break;
                            case "LAB_NOT_ASSIGNED":
                                btnIniciar.enabled = true;
                                sesion_mensaje_error.text = "";
                                AbrirPanel(panelMensajes.gameObject);
                                botonesAulaUno.SetActive(true);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("The laboratory is not assigned to the student. Please ask your teacher.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("O laboratório não está liberado para o estudante. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Öğrenciye laboratuvar tahsis edilmedi. Lütfen öğretmeninize sorun.");
                                        break;
                                }
                                break;
                            case "LOGIN_OK":
                                aula_name = _data["name"].Value;
                                aula_last_name = _data["last_name"].Value;
                                aula_class_group = _data["class_group"].Value;
                                aula_school_name = _data["school_name"].Value;

                                simulador.simuladorActivo.reporte.GuardarDatosSesion(_data["name"].Value, _data["class_group"].Value, _data["class_group"].Value);
                                hud_Usuario.text = _data["name"];
                                hud_Intentos.text = "0";
                                AbrirPanel(panelInicioSesion.gameObject, false);
                                simulador.control.SetActiveFirstPersonController(true);
                                Datos_Usuario.text = _data["name"];
                                break;
                            case "DB_EXCEPTION":
                                btnIniciar.enabled = true;
                                sesion_mensaje_error.text = "";
                                AbrirPanel(panelMensajes.gameObject);
                                botonesAulaUno.SetActive(true);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("Database error. Please contact your provider.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Error en la base de datos. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Erro na base de dados. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Veritabanı hatası. Lütfen sağlayıcınıza başvurun.");
                                        break;
                                }
                                break;
                            case "LICENSE_EXPIRED":
                                btnIniciar.enabled = true;
                                sesion_mensaje_error.text = "";
                                AbrirPanel(panelMensajes.gameObject);
                                botonesAulaUnoOffline.SetActive(true);
                                switch (IdiomaActual)
                                {
                                    case "Ingles":
                                        textoMensaje.text = controlIdiomas.Traductor("There is a problem with your licence, will continue without connecting to the teacher's device.");
                                        break;
                                    case "Espanol":
                                        textoMensaje.text = controlIdiomas.Traductor("Hay un problema con la licencia del gestor de aula. Se ingresará en modo offline.");
                                        break;
                                    case "Portugues":
                                        textoMensaje.text = controlIdiomas.Traductor("Há um problema com a licença do gestor da sala. O ingresso será no modo offline.");
                                        break;
                                    case "Turco":
                                        textoMensaje.text = controlIdiomas.Traductor("Lisansınız ile ilgili bir sorun var, öğretmenin cihazına bağlanmadan devam edecek.");
                                        break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        btnIniciar.enabled = true;
                        sesion_mensaje_error.text = "";
                        AbrirPanel(panelMensajes.gameObject);
                        botonesAulaUno.SetActive(true);
                        switch (IdiomaActual)
                        {
                            case "Ingles":
                                textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                                break;
                            case "Espanol":
                                textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                                break;
                            case "Portugues":
                                textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                                break;
                            case "Turco":
                                textoMensaje.text = controlIdiomas.Traductor("Sunucu yanıtı geçersiz. Lütfen sağlayıcınıza başvurun.");
                                break;
                        }
                    }
                }
                else
                {
                    btnIniciar.enabled = true;
                    sesion_mensaje_error.text = "";
                    AbrirPanel(panelMensajes.gameObject);
                    botonesAulaUno.SetActive(true);
                    switch (IdiomaActual)
                    {
                        case "Ingles":
                            textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                            break;
                        case "Espanol":
                            textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                            break;
                        case "Portugues":
                            textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                            break;
                        case "Turco":
                            textoMensaje.text = controlIdiomas.Traductor("Sunucu yanıtı geçersiz. Lütfen sağlayıcınıza başvurun.");
                            break;
                    }
                }
            }
            else
            {
                btnIniciar.enabled = true;
                sesion_mensaje_error.text = "";
                AbrirPanel(panelMensajes.gameObject);
                botonesAulaUno.SetActive(true);
                switch (IdiomaActual)
                {
                    case "Ingles":
                        textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                        break;
                    case "Espanol":
                        textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                        break;
                    case "Portugues":
                        textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                        break;
                    case "Turco":
                        textoMensaje.text = controlIdiomas.Traductor("Sunucu yanıtı geçersiz. Lütfen sağlayıcınıza başvurun.");
                        break;
                }
            }

        }
        else
        {
            //TODO: Error in server
            btnIniciar.enabled = true;
            sesion_mensaje_error.text = "";
            AbrirPanel(panelMensajes.gameObject);
            botonesAulaDos.SetActive(true);
            switch (IdiomaActual)
            {
                case "Ingles":
                    textoMensaje.text = controlIdiomas.Traductor("It was not possible to connect to the teacher's device. Click RETRY to try to connect again or click OFFLINE to continue without connecting to the teacher's device.");
                    break;
                case "Espanol":
                    textoMensaje.text = controlIdiomas.Traductor("No se ha podido establecer la conexión con el equipo del profesor. Haga clic en REINTENTAR para tratar de realizar la conexión nuevamente o haga clic en OFFLINE para continuar sin conectarse al equipo profesor.");
                    break;
                case "Portugues":
                    textoMensaje.text = controlIdiomas.Traductor("Não foi possível estabelecer conexão com o equipamento do professor. Clique em TENTAR OUTRA VEZ para tentar realizar a conexão novamente ou clique em OFFLINE para continuar sem se conectar com o equipamento do professor.");
                    break;
                case "Turco":
                    textoMensaje.text = controlIdiomas.Traductor("Öğretmenin cihazına bağlanılamadı. Yeniden bağlanmayı denemek için TEKRAR DENE'yi tıklatın veya öğretmenin cihazına bağlanmadan devam etmek için ÇEVRIMDIŞI'u tıklatın.");
                    break;
            }
        }
    }

    public void cerrarVentanaAula()
    {
        FindObjectOfType<Reporte>().botonTerminarReporte.SetActive(true);
        AbrirPanel(panelMensajes.gameObject, false);
        botonesAulaUno.SetActive(false);
    }

    public void ReintentarAula()
    {
        botonesAulaDos.SetActive(false);
        AbrirPanel(panelMensajes.gameObject, false);
        aulaLoginRequest();
    }

    public void OfflineModo()
    {
        botonesAulaDos.SetActive(false);
        var_offline = true;
        AbrirPanel(panelMensajes.gameObject, false);

        sesion_mensaje_error.enabled = false;
        sesion_mensaje_error.text = "";
        btnIniciar.enabled = true;

        sesion_institucion_bg.SetActive(true);
        sesion_Institucion_label.SetActive(true);
        sesion_Institucion.enabled = true;

        sesion_id_curso_bg.SetActive(true);
        sesion_IdCurso_label.SetActive(true); 

        sesion_curso_bg.SetActive(true);
        sesion_user_bg.SetActive(true);

        sesion_curso.inputType = InputField.InputType.Standard;

		if (simulador.ModoMonoUsuario == true) {

            sesion_curso.inputType = InputField.InputType.Standard;

            sesion_usuario.text = simulador.mono_user_name;
            sesion_Institucion.text = simulador.mono_user_institution;

            sesion_curso.readOnly = false;

            switch (IdiomaActual)
			{
			case "Ingles":
				sesion_curso.text = "Chemistry";
				sesion_curso_label.text = "Course";
                    sesion_IdCurso.text = "Course ID";
                    
                    break;
			case "Espanol":
				sesion_curso.text = "Química";
				sesion_curso_label.text = "Curso";
                    sesion_IdCurso.text = "ID Curso";

                    break;
			case "Portugues":
				sesion_curso.text = "Química";
				sesion_curso_label.text = "Curso";
                    sesion_IdCurso.text = "ID Curso";

                    break;
            case "Turco":
                sesion_curso.text = "Kimya";
                sesion_curso_label.text = "Ders";
                    sesion_IdCurso.text = "Ders kimliği";
                    
                    break;
			}

            sesion_curso_bg.GetComponent<Text>().text = "";

			GameObject.FindObjectOfType<ControlIdiomas> ().MtdtradusirInputs ();
		} else {
			switch (IdiomaActual)
			{
			case "Ingles":
				sesion_usuario.text = "Guest";
				sesion_curso.text = "Chemistry";
				sesion_curso_label.text = "Course";
				break;

			case "Espanol":
				sesion_usuario.text = "Invitado";
				sesion_curso.text = "Química";
				sesion_curso_label.text = "Curso";
				break;

			case "Portugues":
				sesion_usuario.text = "Convidado";
				sesion_curso.text = "Química";
				sesion_curso_label.text = "Curso";
				break;

            case "Turco":
                sesion_usuario.text = "Konuk";
                sesion_curso.text = "Kimya";
                sesion_curso_label.text = "Ders";
                break;
			}
		}
    }

    public GameObject btnReportePracticaLibre;

    // Mostrar el Reporte en la practica Libre
    public void ClickVerReportePracticaLibre()
    {
        AbrirPanel(panelMensajes.gameObject, false);
        AccionarPantalla();
        ejecutandoEjercicio = false;

        ActualizarDatosPantalla();
    }

    public void MostrarResultadoEjercicio(bool aprobo)
    {

#if MODO_RAPIDO
		        aprobo = true;
#endif

        if (pantallaActiva)
        {
            Cerrar();
        }

        btnInformacion.SetActive(false);
        btnReportePracticaLibre.gameObject.SetActive(false);

        if (aprobo)
        {
            pantallaBloqueada = true;
            textoMensaje.text = simulador.simuladorActivo.textoMensajeAprobo;

            if (practicaLibre)
            {
                string adicionTexto ="puede ir a generar el reporte de esta práctica o realizar un nuevo intento. haga clic en aceptar para continuar.";
                btnContinuar.gameObject.SetActive(false);
                btnReintentar.gameObject.SetActive(false);
                btnReportePracticaLibre.gameObject.SetActive(true);
                textoMensaje.text += controlIdiomas.Traductor(adicionTexto);
                btnReintentar.transform.GetChild(0).GetComponent<Text>().text = controlIdiomas.Traductor("Aceptar");
            }
            else
            {
                btnReportePracticaLibre.gameObject.SetActive(false);
                btnContinuar.gameObject.SetActive(true);
                btnReintentar.gameObject.SetActive(false);
            }
        }
        else
        {
            //si es practica se reinicia sin mostrar mensajes
            if (practicaLibre)
            {
                textoMensaje.text = "";
                string adicionTexto = "Puede ir a generar el reporte de esta práctica o realizar un nuevo intento. Haga clic en aceptar para continuar.";
                btnContinuar.gameObject.SetActive(false);
                btnReintentar.gameObject.SetActive(false);
                btnReportePracticaLibre.gameObject.SetActive(true);
                textoMensaje.text += controlIdiomas.Traductor(adicionTexto);
                btnReintentar.transform.GetChild(0).GetComponent<Text>().text = controlIdiomas.Traductor("Aceptar");
            }
            else
            {

                btnReintentar.transform.GetChild(0).GetComponent<Text>().text = controlIdiomas.Traductor("Reintentar");
                textoMensaje.text = simulador.simuladorActivo.textoMensajeNoAprobo;
                btnContinuar.gameObject.SetActive(false);
                btnReintentar.gameObject.SetActive(true);
            }
        }

        AbrirPanel(panelInicial.gameObject);
        AbrirPanel(panelMensajes.gameObject);
        AbrirPanel(panelEjercicios, false);
        AbrirPanel(simulador.simuladorActivo.indicadores, false);
    }

    //se ha buggeado el script UIButton de los toggle, si se quita esta funcion sale error apesar de que ya se le quito la referencia (none)
    public void SeleccionarRespuesta()
    {

    }

    public void MostrarEvaluacion()
    {
        evaluacion.DesordenarPreguntas();
    }

    public void BtnTerminar()
    {
        if (!practicaLibre && !evaluacion.AgregarRespuestas())
        {
            Debug.Log("termine de contestar");
            return;
        }
        simulador.simuladorActivo.ActualizarDatosReporte();
        panelEvaluacion.gameObject.SetActive(false);
        AbrirPanel(panelMensajes.gameObject);
        switch (IdiomaActual)
        {
            case "Ingles":
                textoMensaje.text = controlIdiomas.Traductor("Congratulations! You have completed the proposed challenge. Click “Continue” to generate the laboratory report.");
                break;

            case "Espanol":
                textoMensaje.text = controlIdiomas.Traductor("¡Felicitaciones! ha completado el reto propuesto, haga clic en el botón continuar para generar el reporte de laboratorio.");
                break;

            case "Portugues":
                textoMensaje.text = controlIdiomas.Traductor("Parabéns! Você concluiu o desafio proposto, clique no botão continuar para gerar o relatório da experiência.");
                break;

            case "Turco":
                textoMensaje.text = controlIdiomas.Traductor("Tebrikler! Önerilen meydan okumayı tamamladınız. Laboratuvar raporunu oluşturmak için 'Devam' i tıklayın.");
                break;
        }
        simulacionTerminada = true;
        ejecutandoEjercicio = false;
        btnContinuar.gameObject.SetActive(true);
        simulador.pantalla.ActualizarDatosPantalla();
    }

    public void AccionarPantalla()
    {
        AbrirPanel(panelEntradaDatos, false);
        AbrirPanel(btnInformacion, false);
        AbrirPanel(panelIntentosReloj, false);
        AbrirPanel(btnSalir.gameObject, false);
        pantallaActiva = true;
        AbrirCompartimiento();
    }

    public void AbrirCompartimiento()
    {
        iTween.RotateTo(
            refCompartimientoMesa3D,
            iTween.Hash(
            "rotation", rotacionFinalCompartiemiento,
            "islocal", true,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 0.5f,
            "onComplete", "SacarPantalla",
            "onCompleteTarget", gameObject
            )
        );
    }

    public void SacarPantalla()
    {
        refPantalla3D.SetActive(true);
        iTween.MoveTo(
            refPantalla3D,
            iTween.Hash(
            "position", posicionPantallaAfuera,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 0.5f,
            "onComplete", "DesplazarPantalla",
            "onCompleteTarget", gameObject
            )
        );
    }

    void DesplazarPantalla()
    {
        Camera cam = Camera.main;
        float pos = (cam.nearClipPlane + 0.01f);
        float h = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f;
        EscalaFinalPantalla = new Vector3(h * cam.aspect, 0.001f, (h - h / 7));
        posicionFinalPantalla = Camera.main.transform.position + Camera.main.transform.TransformDirection(Vector3.forward) * 0.345f;

        iTween.MoveTo(
            refPantalla3D,
            iTween.Hash(
            "position", posicionFinalPantalla,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 1
            )
        );
        // Cambiar el alpha de la pantalla 3d
        refPantalla3D.GetComponent<MeshRenderer>().material.color = Color.white;
        iTween.FadeTo(refPantalla3D,
                       iTween.Hash(
            "alpha", 0,
            "delay", 0.5f,
            "easeType", iTween.EaseType.easeInOutCubic,
            "time", 0.5f
            )
                       );
        
        iTween.ValueTo(panelPantalla.gameObject,
                        iTween.Hash(
            "from", 0,
            "to", 1,
            "delay", 0.5f,
            "easeType", iTween.EaseType.easeInOutCubic,
            "time", 0.5f,
            "onupdate", "OnUpdateAlpha",
            "onupdatetarget", gameObject
            )
                        );
        //Darle tiempo a el desplazamiento para que no se transpase la pantalla por la mesa
        StartCoroutine(EscalarPantalla());
    }

    void OnUpdateAlpha(float newAlpha)
    {
        //panelPantalla.alpha = newAlpha; hadron games
    }

    IEnumerator EscalarPantalla()
    {
        refPantalla3D.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        iTween.ScaleTo(
            refPantalla3D,
            iTween.Hash(
            "scale", EscalaFinalPantalla,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 1f,
            "oncomplete", "TerminoEscalarPantalla",
            "oncompletetarget", gameObject
            )
        );
    }

    public void TerminoEscalarPantalla()
    {
        refPantalla3D.SetActive(false);
        panelPantalla.gameObject.SetActive(true);
        Color c = refPantalla3D.GetComponent<Renderer>().material.color;
        c.a = 1;
        refPantalla3D.GetComponent<Renderer>().material.color = c;
        refPantalla3D.SetActive(false);
        panelPantalla.gameObject.SetActive(true);
        Grafico();
        DibujarGrafico(false);
    }

    public void DibujarGrafico(bool dibujarRapido)
    {
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
        {
            float[] datos = simulador.simuladorActivo.ObtenerDatosGrafica();
            int tamArreglo = datos.Length / 3;
            float[] datosTiempo = new float[tamArreglo];
            float[] datosTemperatura = new float[tamArreglo];
            float[] datosPorcentageAlcohol = new float[tamArreglo];
            Array.Copy(datos, datosTiempo, datosTiempo.Length);
            Array.Copy(datos, tamArreglo, datosTemperatura, 0, datosTiempo.Length);
            Array.Copy(datos, tamArreglo * 2, datosPorcentageAlcohol, 0, datosTiempo.Length);
            grafico2D.DibujarFermentacion(ref datosTiempo, ref datosTemperatura, ref datosPorcentageAlcohol);
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
        {
            float[] datos = simulador.simuladorActivo.ObtenerDatosGrafica();
            int tamArreglo = datos.Length / 3;
            float[] datosTiempo = new float[tamArreglo];
            float[] datosTemperatura = new float[tamArreglo];
            float[] datosPorcentageAlcohol = new float[tamArreglo];
            Array.Copy(datos, datosTiempo, datosTiempo.Length);
            Array.Copy(datos, tamArreglo, datosTemperatura, 0, datosTiempo.Length);
            Array.Copy(datos, tamArreglo * 2, datosPorcentageAlcohol, 0, datosTiempo.Length);
            grafico2D.DibujarDestilacion(ref datosTiempo, ref datosTemperatura, ref datosPorcentageAlcohol);
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorAminoAcido))
        {
            float[] datos = simulador.simuladorActivo.ObtenerDatosGrafica();
            int tamArreglo = datos.Length / 2;
            float[] datosTiempo = new float[tamArreglo];
            float[] datosTemperatura = new float[tamArreglo];
            Array.Copy(datos, datosTiempo, datosTiempo.Length);
            Array.Copy(datos, tamArreglo, datosTemperatura, 0, datosTiempo.Length);
            grafico2D.DibujarAminoacido(ref datosTiempo, ref datosTemperatura);
        }
    }

    public void DesactivarPantalla()
    {
        pantallaActiva = true;

        refPantalla3D.SetActive(true);
        iTween.MoveTo(
            refPantalla3D,
            iTween.Hash(
            "position", posicionPantallaAfuera,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 1,
            "oncomplete", "GuardarPantalla",
            "oncompletetarget", gameObject
            )
        );

        iTween.ScaleTo(refPantalla3D, escalaInicialPantalla, 1);
    }


    public void GuardarPantalla()
    {
        refPantalla3D.SetActive(true);
        iTween.MoveTo(
            refPantalla3D,
            iTween.Hash(
            "position", posicionPantallaAdentro,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 0.5f,
            "oncomplete", "CerrarCompartimiento",
            "oncompletetarget", gameObject
            )
        );
    }


    public void CerrarCompartimiento()
    {
        print("Cerrar compartimiento");
        iTween.RotateTo(
            refCompartimientoMesa3D,
            iTween.Hash(
            "rotation", rotacionInicialCompartiemiento,
            "islocal", true,
            "looktarget", Camera.main,
            "easeType", "easeInOutCubic",
            "time", 0.5f,
            "oncomplete", "TerminoCerrarCompartimiento",
            "oncompletetarget", gameObject
            )
        );
    }

    void TerminoCerrarCompartimiento()
    {
        refPantalla3D.SetActive(false);
        TerminoAnimacionGuardarPantalla();
    }

    public void TerminoAnimacionGuardarPantalla()
    {
        refPantalla3D.SetActive(false);
        pantallaActiva = false;
        simulador.simuladorActivo.botonPantalla.Normalizar();

        if (simulacionTerminada)
        {
            pantallaActiva = true;
            AbrirPanel(panelReporte.gameObject);
            hoja1Reporte.SetActive(true);
            hoja2Reporte.SetActive(false);

            // Se desactiva el boton 
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.OSXEditor)
                botonTerminarReporte.SetActive(false);

            if (simulador.simuladorActivo.practicaLibre)
            {
                botonAtrasReporte.SetActive(false);
                botonAdelanteReporte.SetActive(false);

                botonTerminarReporte.transform.parent = hoja1Reporte.transform;
                btnVisualizarPDF.gameObject.transform.parent = hoja1Reporte.transform;
                btnEnviarCorreo.gameObject.transform.parent = hoja1Reporte.transform;

                parametrosReporteAula();

                StartCoroutine(reporte.CrearImagen(true, true));  
            }
            else
            {
                parametrosReporteAula();

                StartCoroutine(reporte.CrearImagen());

                botonAtrasReporte.SetActive(false);
                botonAdelanteReporte.SetActive(true);

                botonTerminarReporte.transform.parent = hoja2Reporte.transform;
                btnVisualizarPDF.gameObject.transform.parent = hoja2Reporte.transform;
                btnEnviarCorreo.gameObject.transform.parent = hoja2Reporte.transform;
            }

            btnVisualizarPDF.gameObject.SetActive(false);
            btnEnviarCorreo.gameObject.SetActive(false);

#if UNITY_IPHONE || UNITY_IOS || UNITY_ANDROID//hadron, este if estaba en ambas partes del if anterior
		btnVisualizarPDF.gameObject.SetActive(true);
		btnEnviarCorreo.gameObject.SetActive(true);
#endif
        }
    }

    private void parametrosReporteAula()
    {
        if (!practicaLibre)
        {
            if (var_aula == true)
            {
                reporte.aula_mode = true;
                reporte.user_report = sesion_usuario.text;

                switch (simulador.simuladorActivo.name)
                {
                    case "Simulador Fermentacion Alcoholica":
                        reporte.lab_code_report = "QUIS3D101";
                        break;
                    case "Simulador Destilacion Alcoholica":
                        reporte.lab_code_report = "QUIS3D105";
                        break;
                    case "Simulador Sintesis Aminoacidos":
                        reporte.lab_code_report = "QUIS3D103";
                        break;
                }
            }
            else
            {
                reporte.aula_mode = false;
            }
        }
        else
        {
            reporte.aula_mode = false;
        }
    }

    //Habilita boton Terminar para que pueda salir sin antes descargar la imagen
    public void ActivarBotonTerminar()
    {
        print("");
        reporte.ActivarBotonesReporte(true);
    }

    public void MostrarMensajeFermentacion(string mensaje, int lugar = 0)
    {
        mensaje = controlIdiomas.Traductor(mensaje);
        textoMensajeFermentacion[lugar].SetActive(true);
        textoMensajeFermentacion[lugar].GetComponentInChildren<Text>().text = mensaje;
        StartCoroutine(DesactivarMensajeFermentacion(lugar));
    }

    public IEnumerator DesactivarMensajeFermentacion(int _lugar, float _tiempo = 10)
    {
        yield return new WaitForSeconds(_tiempo);
        textoMensajeFermentacion[_lugar].GetComponentInChildren<Text>().text = "";
        textoMensajeFermentacion[_lugar].SetActive(false);
    }

    public void BtnSeleccionarAtras()
    {
        Debug.Log(IdiomaActual);
        switch (controlIdiomas.idioma)
        {
            case ControlIdiomas.Idioma.Ingles:
                TextoPracticaLibre = "Free practice";
                TextoReto = "Challenge";
                break;

            case ControlIdiomas.Idioma.Espanol:
                TextoPracticaLibre = "Practica Libre";
                TextoReto = "Reto";
                break;

            case ControlIdiomas.Idioma.Portugues:
                TextoPracticaLibre = "Prática livre";
                TextoReto = "Desafio";
                break;

            case ControlIdiomas.Idioma.Turco:
                TextoPracticaLibre = "Serbest alıştırma";
                TextoReto = "İnceleme";
                break;
        }

        labelRetoPractica.text = TextoReto;
    }

    public void BtnVisualizarPDF()
    {
        print("BtnVisualizarPDF");
        reporte.VisualizarPDF();
    }

    public void BtnEnviarPDF()
    {
        reporte.Enviar_PDF_Correo();
    }

    public void BtnSeleccionarAdelante()
    {
        Debug.Log(IdiomaActual);
        switch (IdiomaActual)
        {
            case "Ingles":
                TextoPracticaLibre = "Free practice";
                TextoReto = "Challenge";
                break;

            case "Espanol":
                TextoPracticaLibre = "Práctica Libre";
                TextoReto = "Reto";
                break;

            case "Portugues":
                TextoPracticaLibre = "Prática livre";
                TextoReto = "Desafio";
                break;

            case "Turco":
                TextoPracticaLibre = "Serbest alıştırma";
                TextoReto = "İnceleme";
                break;
        }

        labelRetoPractica.text = TextoPracticaLibre;
    }

    public void BtnAdelanteReporte()
    {
        //Habilitar botones ver pdf y enviar corro para moviles
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        {
            AbrirPanel(btnVisualizarPDF.gameObject);
            AbrirPanel(btnEnviarCorreo.gameObject);
        }

        hoja1Reporte.SetActive(false);
        hoja2Reporte.SetActive(true);

        if (!guardadoHoja2Reporte)
        {
            parametrosReporteAula();
            StartCoroutine(reporte.CrearImagen(false, false));
            guardadoHoja2Reporte = true;
        }
    }

    public void BtnAtrasReporte()
    {
        hoja1Reporte.SetActive(true);
        hoja2Reporte.SetActive(false);
    }

    void CrearLabelTablaDatos(string texto, int posFila, float posRelX, float posRelY)
    {
        GameObject _refLabel = Instantiate(refLabel, hoja1Reporte.transform);
        _refLabel.transform.GetChild(0).GetComponent<Text>().text = controlIdiomas.Traductor(texto);
        _refLabel.GetComponent<RectTransform>().localPosition = new Vector2(posRelX, posRelY - (posFila) * 30f);
        listaTablaDatos.Add(_refLabel);
    }

    public void ActualizarTablaDatos(List<string> textos, List<string> datos, string _preguntas)
    {
        for (int i = 0; i < textos.Count; i++)
            CrearLabelTablaDatos(textos[i], i, -120, -180);
        for (int i = 0; i < datos.Count; i++)
            CrearLabelTablaDatos(datos[i], i, 120, -180);

        preguntas.text = _preguntas;

        List<bool> _resp_esperadas = new List<bool>();
        float _preg_correctas = 0;

        Debug.Log(simulador.simuladorActivo.name);
        if (!practicaLibre)
        {

            for (int y = 0; y < simulador.simuladorActivo.reporte.respuestas.Count; y++)
            {
                string[] _texts = simulador.simuladorActivo.reporte.preguntas[y].Split(':');
                bool _resp = false;
                if (_texts[1] == "true")
                {
                    _resp = true;
                }

                if (simulador.simuladorActivo.reporte.respuestas[y] == _resp)
                {
                    _preg_correctas = _preg_correctas + 1;
                }
            }
        }

        //No se muestran las preguntas orientadoras cuando esta en practica libre
        if (!practicaLibre)
        {
            preguntasOrientadoras.text = simulador.simuladorActivo.preguntasOrientadoras;
            AbrirPanel(texto2preguntasOrientadoras.gameObject);
        }
        else
        {
            texto2preguntasOrientadoras.gameObject.SetActive(false);
            preguntasOrientadoras.text = "N/A";
            preguntas.text = "N/A";
        }

        reporte_Tiempo.text = ref_Tiempo.text.ToString();

        if (var_aula == true && var_offline == false)
        {
            reporte_Usuario.text = aula_name + " " + aula_last_name;
            reporte_idCurso.text = aula_class_group;
            reporte_institucion.text = aula_school_name;

            switch (IdiomaActual)
            {
                case "Ingles":
                    reporte_Curso.text = "Chemistry";
                    break;
                case "Espanol":
                    reporte_Curso.text = "Química";
                    break;
                case "Portugues":
                    reporte_Curso.text = "Química";
                    break;
                case "Turco":
                    reporte_Curso.text = "Kimya";
                    break;
            }
        }
        else
        {
            reporte_Usuario.text = sesion_usuario.text;
            reporte_Curso.text = sesion_curso.text;
            reporte_idCurso.text = sesion_IdCurso.text;
            reporte_institucion.text = sesion_Institucion.text;
        }

        reporte_Situacion.text = simulador.simuladorActivo.situacion;
        reporte_intentos.text = simulador.simuladorActivo.intentos.ToString();
        reporte_unidad.text = simulador.simuladorActivo.unidad;

        if (!practicaLibre)
        {
            _cal = (simulador.simuladorActivo.intentos - 1);
            _cal = 1 - _cal / 4;
            if (_cal < 0)
            {
                _cal = 0;
            }

            Debug.Log(_cal);

            _cal = 3 + _cal;

            _cal = _cal + _preg_correctas / 5;

            Debug.Log(_cal);

            reporte_calificacion.text = FindObjectOfType<ScoreController>().GetScoreFromFactor01(_cal / 5f);

			//SENT LTI SCORE
			if (simulador.tipoAplicacion == Simulador.TipoAplicacion.WEB && simulador.ModoLTI == true) {
				float cal_percentage = _cal / 5;
				string _url = FindObjectOfType<Simulador> ().url_score_lti + "?score=" + cal_percentage.ToString () + "&lti_params=" + simulador.parametros_lti;
				Debug.Log(_url);
				WWW wwwLTI = new WWW(_url);
				StartCoroutine(WaitForRequestAulaLTI(wwwLTI));
			}
        }
        else
        {
            reporte_calificacion.text = "N/A";
        }
    }

    public void ActualizarDatosPantalla()
    {
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
        {
            pantalla_Azucar.text = azucarTexto.text;
            pantalla_Levadura.text = levaduraTexto.text;
            pantalla_Acido.text = acidoTexto.text;
            pantalla_Base.text = baserTexto.text;
            pantalla_Temperatura.text = temperaturaTexto.text;
            pantalla_Rpm.text = rpmTexto.text;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
        {
            pantalla_Solucion_Destilacion.text = solucionDestilacionTexto.text;
            pantalla_Acido_Destilacion.text = acidoDestilacionTexto.text;
            pantalla_Base_Destilacoin.text = baserDestilacionTexto.text;
            pantalla_Temperatura_Destilacion.text = temperaturaDestilacionTexto.text;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorAminoAcido))
        {
            float[] data = new float[5];
            data = simulador.simuladorActivo.GetComponent<SimuladorAminoAcido>().GetDataScreen();

            screen_solutionA.text = data[0].ToString();
            screen_solutionB.text = data[1].ToString();
            screen_solutionC.text = data[2].ToString();
            screen_solutionD.text = data[3].ToString();
            screen_solvent.text = data[4].ToString();

            screen_agitator.text = input_agitatorSpeed.text;
            screen_temperature.text = input_temperature.text;
        }
    }

	IEnumerator WaitForRequestAulaLTI(WWW www)
	{
		yield return www;
	}
}