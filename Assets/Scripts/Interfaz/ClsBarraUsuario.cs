﻿using UnityEngine;

public class ClsBarraUsuario : MonoBehaviour
{
    [SerializeField]
    private Animator animatorBarraInformacionUsuario;

    public void OnButtonDesplegarContraerMenu()
    {
        animatorBarraInformacionUsuario.SetBool("desplegar", !animatorBarraInformacionUsuario.GetBool("desplegar"));
    }
}
