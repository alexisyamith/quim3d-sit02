﻿

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/* Objetivo  :  Fermentar o sacar 45 % de alcohol 

ph < 5 

Claves : 
base: es una solución para subir  el PH y ácido: es una solución para bajar el PH

acido se hecha proporcionalmente  por cada litro de azucar
al igual que la base

porcentaje_Temperatura_Buena = 5;

agitador 100 rpms 

 */

//Clase Principal
public class SimuladorFermentacion : SimuladorGenerico
{
    private int porcentaje_Alcohol_Final = 0;
    private int tiempo_Adicional = 0;
    private int tiempo_Adicional_X_RPM = 30;
    private int tiempo_Adicional_X_AZUCAR = 30;

    //Porcentajes deducibles segun proceso
    private int porcentaje_Poca_Levadura = 15;
    private int porcentaje_Demasiada_Levadura = 20;

    private int porcentaje_Temperatura_Ninguna = 20;
    private int porcentaje_Temperatura_Buena = 5;
    private int porcentaje_Temperatura_Muerte = 30;

    private int porcentaje_PH_Ninguna = 20;
    private int porcentaje_PH_Muerte = 30;

    //Rango de valores para el funcionamiento
    private float valorMinimoAptoLevadura = 2.5f;
    private float valorMaximoAptoLevadura = 3.5f;

    private int valorMinimoAptoAgitador = 99;
    private int valorMaximoAptoAgitador = 101;

    private int valorMinimoTemperaturaBuena = 23;
    private int valorMaximoTemperaturaBuena = 32;
    private int valorTemperaturaOptima = 25;

    private float valorMinimoPH = 3;
    private float valorMaximoPH = 6;

    //Limite capacidad de valores
    private int rpm_Maxima = 200;
    private int rpm_Minima = 0;
    private int temperatura_Maxima = 100;
    private int temperatura_Minima = 20;
    private int tiempo_Disponible = 240;

    //private int tiempo_Disponible = 10;
    private int tiempo_Usuario;

    //Resultados Fermentacion
    private enum Tipo_Fermentacion_X_PH { FERMENTACION_NINGUNA, FERMENTACION_OPTIMA, MUERTE_BACTERIAS }
    private Tipo_Fermentacion_X_PH tipo_Fermentacion_X_PH;

    private enum Tipo_Fermentacion_X_Temperatura { FERMENTACION_NINGUNA, FERMENTACION_BUENA, FERMENTACION_OPTIMA, MUERTE_BACTERIAS }
    private Tipo_Fermentacion_X_Temperatura tipo_Fermentacion_X_Temperatura;

    private enum Nivel_Cultivo { BAJO = 20, MEDIO = 50, ALTO = 100 }
    private Nivel_Cultivo nivel_Cultivo;

    //Indicadores
    private float PH;
    private float rpm;
    private float temperatura;
    private float alcoholEtilico;
    private float presion;
    private float gasCarbonico;
    private float _porcentajeAlcoholGrafica;

    //Factores para adicionar insumos
    public float factorAgregarLiquido = 0.1f;
    private float factorAdicionarTemperatura = 0.05f;
    private float factorAdicionarColorLevadura = 0.023f;

    //Valvulas y Pulsadores
    public GameObject btnPantalla;
    public GameObject btnAzucar;
    public GameObject btnLevadura;
    public GameObject btnAcido;
    public GameObject btnBase;
    public GameObject btnTemperaturaMas;
    public GameObject btnTemperaturaMenos;
    public GameObject btnMixerMas;
    public GameObject btnMixerMenos;
    public GameObject btnVaciarTanque;
    public GameObject btnEncenderMotor;

    //FUNCIONAMIENTO GENERAL
    private bool motorEncendido = false;
    public bool vaciarTanque = false;
    public bool tiempoSimulacionCorrecta = false;
    private float tiempoActual;
    private string tiempoSimulacion = "0";      //Verificar si se coloca en la pantalla
    private float alcoholResultante;

    //Delegados
    public delegate void Actualizar();
    public Actualizar _estado;

    //Ingredientes
    public Ingrediente azucar;
    public Ingrediente levadura;
    public Ingrediente _base;
    public Ingrediente acido;

    //Tanque
    public Tanque tanque;

    //Colores Cultivo
    private Color colorVerde = Color.green;
    private Color colorAmarillo = Color.yellow;
    private Color colorRojo = Color.red;
    private Color colorBlanco = Color.white;

    //Colores Temperatura
    public Renderer resistencia;
    private Material materialResistencia;
    private Color _colorActual;
    private bool animandoTemperatura = false;

    //Color Tanque referencia
    private Material materialTanque;

    //Mensajes
    private string msjCerrarValvulaEscape = "Antes de ingresar insumos desactive la válvula de alivio.";
    private string msjTiempoExtraAzucar = "Debes reiniciar, revisa los parametros";
    private string msjTiempoExtraRPM = "Error en las RPM el proceso se demorara 30 minutos mas.";
    private string msjEnciendaAgitador = "Primero debe encender el agitador.";

    //Agitador
    public GameObject agitador;
    private float factorVelocidadAgitador = 6;

    //Cambio colores del tanque
    private Color colorActualTanque;

    //Cambio a color verde agitado
    private bool colorLerpInicialGuardada = false;
    private float tiempoActualColorInicial = 0;
    private bool solucionTotalmenteAgitada;
    private Color colorTotalmenteAgitado;
    private float duracionCambioColorAgitado = 120;

    //Cambio a color Inicial
    private bool colorLerpAgitadoGuardado = false;
    private float tiempoActualColorAgitado = 0;
    private Color colorSolucionInicialAgitador;
    private Color colorSolucionInicial;
    private float duracionCambioColorVaciado = 20;

    //Referencia a script animacion botones
    private AnimacionBotones scriptBtnTempAgregar;
    private AnimacionBotones scriptBtnTempDisminuir;
    private AnimacionBotones scriptBtnRPMAgregar;
    private AnimacionBotones scriptBtnRPMDisminuir;
    private AnimacionBotones scriptBtnAgitador;
    private AnimacionBotones scriptBtnValvulaAlivio;

    public struct DatosGrafico
    {
        public float tiempo;
        public float temperatura;
        public float porcentajeAlcohol;
    };

    private List<DatosGrafico> datosGrafico;

    // Diccionario de eventos para los botones 3D
    private Dictionary<GameObject, Action> dictEventosTouch = new Dictionary<GameObject, Action>();

    public GameObject btnReset;

    public string estado;
    private float lerpAlcohol = 0;

    private bool purgado_Tiempo_X_Agitador;
    private bool purgado_Tiempo_X_Azucar;

    // Use this for initialization
    private void Start()
    {
        datosGrafico = new List<DatosGrafico>();

        msjCerrarValvulaEscape = controlIdiomas.Traductor(msjCerrarValvulaEscape);
        msjTiempoExtraAzucar = controlIdiomas.Traductor(msjTiempoExtraAzucar);
        msjTiempoExtraRPM = controlIdiomas.Traductor(msjTiempoExtraRPM);
        msjEnciendaAgitador = controlIdiomas.Traductor(msjEnciendaAgitador);

        //Funcionamiento EasyTouch
        refSeleccionMouse._DltSeleccion += On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion += On_TouchUpBtnFermentacion;

        //Estado Inicial
        CambiarEstado(EstadoSinActividad);
        materialResistencia = resistencia.material;
        materialTanque = tanque.materialTanque;

        //Asignar referencias script botones
        scriptBtnTempAgregar = btnTemperaturaMas.GetComponent<AnimacionBotones>();
        scriptBtnTempDisminuir = btnTemperaturaMenos.GetComponent<AnimacionBotones>();
        scriptBtnRPMAgregar = btnMixerMas.GetComponent<AnimacionBotones>();
        scriptBtnRPMDisminuir = btnMixerMenos.GetComponent<AnimacionBotones>();
        scriptBtnAgitador = btnEncenderMotor.GetComponent<AnimacionBotones>();
        scriptBtnValvulaAlivio = btnVaciarTanque.GetComponent<AnimacionBotones>();

        // agregar eventos para los botones 3d
        dictEventosTouch.Add(btnAzucar.gameObject, ClickBtnAzucar);
        dictEventosTouch.Add(btnLevadura.gameObject, ClickBtnLevadura);
        dictEventosTouch.Add(btnAcido.gameObject, ClickBtnAcido);
        dictEventosTouch.Add(btnBase.gameObject, ClickBtnBase);
        dictEventosTouch.Add(btnTemperaturaMas.gameObject, ClickBtnTempMas);
        dictEventosTouch.Add(btnTemperaturaMenos.gameObject, ClickBtnTempMenos);
        dictEventosTouch.Add(btnMixerMas.gameObject, ClickBtnMixerMas);
        dictEventosTouch.Add(btnMixerMenos.gameObject, ClickBtnMixerMenos);
        dictEventosTouch.Add(btnVaciarTanque.gameObject, ClickBtnVaciarTanque);
        dictEventosTouch.Add(btnEncenderMotor.gameObject, ClickBtnEncendidoMotor);
    }

    private void InicializarValores()
    {
        //Insumos
        azucar.valor = 0;
        simulador.pantalla.azucarTexto.text = "0.00";
        simulador.pantalla.pantalla_Azucar.text = "0";
        azucar.scriptBtn.Normalizar();
        azucar.valvulaAbierta = false;

        levadura.valor = 0;
        simulador.pantalla.levaduraTexto.text = "0";
        simulador.pantalla.pantalla_Levadura.text = "0.00";
        levadura.scriptBtn.Normalizar();
        levadura.valvulaAbierta = false;

        acido.valor = 0;
        simulador.pantalla.acidoTexto.text = "0";
        simulador.pantalla.pantalla_Acido.text = "0.00";
        acido.scriptBtn.Normalizar();
        acido.valvulaAbierta = false;

        _base.valor = 0;
        simulador.pantalla.baserTexto.text = "0";
        simulador.pantalla.pantalla_Base.text = "0.00";
        _base.scriptBtn.Normalizar();
        _base.valvulaAbierta = false;

        //Temperatura
        materialResistencia.color = new Color(1, 0.8f, 0.8f, 1);
        temperatura = 20;
        simulador.pantalla.temperaturaTexto.text = temperatura.ToString();
        simulador.pantalla.pantalla_Temperatura.text = temperatura.ToString();
        scriptBtnTempAgregar.Normalizar();
        scriptBtnTempDisminuir.Normalizar();

        //RPM
        rpm = 0;
        simulador.pantalla.rpmTexto.text = "0";
        simulador.pantalla.pantalla_Rpm.text = "0";
        motorEncendido = false;
        solucionTotalmenteAgitada = false;
        colorTotalmenteAgitado = new Color32(0, 190, 40, 150);
        scriptBtnRPMAgregar.Normalizar();
        scriptBtnRPMDisminuir.Normalizar();

        //Nivel Cultivo
        simulador.pantalla.indicadores_Nivel_Cultivo_Color.color = colorBlanco;
        simulador.pantalla.indicadores_Nivel_Cultivo_Texto.text = "0 %";

        //PH
        PH = 7;
        simulador.pantalla.indicadores_PH.text = "7";

        //Tiempos 
        tiempo_Usuario = tiempo_Disponible;
        simulador.pantalla.indicador_Tiempo_Usuario.text = "00:00:00";
        tiempoActual = 0;
        tiempo_Adicional = 0;
        purgado_Tiempo_X_Agitador = false;
        purgado_Tiempo_X_Azucar = false;

        //Tanque
        tanqueDisponible = true;
        tanque.ReiniciarTanque();
        colorSolucionInicial = tanque.materialTanque.color;

        //Presion
        presion = 0;
        simulador.pantalla.indicadores_Presion.text = presion.ToString("f2");

        //Porcentaje Alcohol
        simulador.pantalla.indicadores_Porcentaje_Alcohol.text = "0";
        _porcentajeAlcoholGrafica = 0;

        //Lerp color solucion
        colorLerpInicialGuardada = false;
        tiempoActualColorInicial = 0;
        colorLerpAgitadoGuardado = false;
        tiempoActualColorAgitado = 0;

        scriptBtnAgitador.Normalizar();
        scriptBtnValvulaAlivio.Normalizar();

        ReiniciarGrafica();
    }

    private void ReiniciarGrafica()
    {
        datosGrafico = new List<DatosGrafico>();
        // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0, porcentajeAlcohol = 0 });
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0, porcentajeAlcohol = 0 });
    }

    // Update is called once per frame
    private void Update()
    {
        _estado();
        estado = _estado.Method.Name;
    }

    //ESTADOS
    //Cambiar Estado
    private void CambiarEstado(Actualizar estado)
    {
        _estado = estado;
    }

    //Estado Inicial
    private void EstadoIniciando()
    {
        ContarTiempoSession();
    }

    //Estado Sin Actividad
    private void EstadoSinActividad()
    {

    }

    //Estado Simulacion
    private void EstadoSimulacion()
    {
        //Cuenta tiempo de la fermentacion
        ContarTiempo();
        //Cuenta tiempo de la Session
        ContarTiempoSession();
        //Calcula valores del ejercicio

        if (tiempoActual > 120)
        {
            CalcularPresion();
            CalcularResultado();
        }

        //Vaciado del  tanque
        if (vaciarTanque)
        {
            if (!tanqueVacio)
            {
                tanqueDisponible = false;

                if (azucar.valor > 0)
                {
                    azucar.valor -= factorAgregarLiquido * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime, true);
                    simulador.pantalla.azucarTexto.text = azucar.valor.ToString("f2");
                }
                else
                {
                    azucar.valor = 0;
                    simulador.pantalla.azucarTexto.text = "0.00";
                }

                if (levadura.valor > 0)
                {
                    levadura.valor -= factorAgregarLiquido * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime, true);
                    simulador.pantalla.levaduraTexto.text = levadura.valor.ToString("f2");


                    if (levadura.valor < 2.5f)
                    {
                        //Cambiar a color inicial 
                        //Guarda solo una vez el color actual, desde donde iniciara el lerp
                        if (!colorLerpInicialGuardada)
                        {
                            colorActualTanque = tanque.materialTanque.color;
                            colorLerpInicialGuardada = true;
                        }

                        tanque.materialTanque.color = Color.Lerp(colorActualTanque, colorSolucionInicial, tiempoActualColorInicial);
                        if (tiempoActualColorInicial < 1)
                            tiempoActualColorInicial += Time.deltaTime / duracionCambioColorVaciado;
                    }
                }
                else
                {
                    levadura.valor = 0;
                    simulador.pantalla.levaduraTexto.text = "0.00";
                    materialTanque.color = colorSolucionInicial;
                    tiempoActualColorAgitado = 0;
                    colorLerpAgitadoGuardado = false;
                    tiempoActualColorInicial = 0;
                    colorLerpInicialGuardada = false;
                }

                if (acido.valor > 0)
                {
                    acido.valor -= factorAgregarLiquido * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime, true);
                    simulador.pantalla.acidoTexto.text = acido.valor.ToString("f2");
                    FijarPH(factorAgregarLiquido);
                }
                else
                {
                    acido.valor = 0;
                    simulador.pantalla.acidoTexto.text = "0.00";
                }

                if (_base.valor > 0)
                {
                    _base.valor -= factorAgregarLiquido * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime, true);
                    simulador.pantalla.baserTexto.text = _base.valor.ToString("f2");
                    FijarPH(-factorAgregarLiquido);
                }
                else
                {
                    _base.valor = 0;
                    simulador.pantalla.pantalla_Base.text = "0.00";
                }
            }
            else
            {
                vaciarTanque = false;
                tanqueDisponible = true;
            }
        }

        //Accionamiento de valvulas
        if (tanqueDisponible)
        {
            //Cuando se abre la valvula del azucar
            if (azucar.valvulaAbierta)
            {
                azucar.valor += factorAgregarLiquido * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime);
                simulador.pantalla.azucarTexto.text = azucar.valor.ToString("f2");
            }
            //Cuando se abre la valvula de la levadura
            if (levadura.valvulaAbierta)
            {
                levadura.valor += factorAgregarLiquido * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime);
                simulador.pantalla.levaduraTexto.text = levadura.valor.ToString("f2");

                //Verifica que tenga menos de la levadura maxima para no adicionar mas color e iniciar otros procesos de color
                if (levadura.valor < valorMinimoAptoLevadura)
                    tanque.materialTanque.color = new Color(materialTanque.color.r, materialTanque.color.g, materialTanque.color.b + (factorAdicionarColorLevadura * Time.deltaTime), materialTanque.color.a);
            }
            //Cuando se abre la valvula del Acido
            if (acido.valvulaAbierta)
            {
                acido.valor += factorAgregarLiquido * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime);
                simulador.pantalla.acidoTexto.text = acido.valor.ToString("f2");
                FijarPH(-factorAgregarLiquido);
            }
            //Cuando se abre la valvula de la Base
            if (_base.valvulaAbierta)
            {
                _base.valor += factorAgregarLiquido * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime);
                simulador.pantalla.baserTexto.text = _base.valor.ToString("f2");
                FijarPH(factorAgregarLiquido);
            }
        }
        //Cerrar valvulas si esta lleno el tanque
        else
        {
            azucar.valvulaAbierta = false;
            levadura.valvulaAbierta = false;
            acido.valvulaAbierta = false;
            _base.valvulaAbierta = false;

            //TODO llamar esto solo una vez
            azucar.scriptBtn.Normalizar();
            levadura.scriptBtn.Normalizar();
            _base.scriptBtn.Normalizar();
            acido.scriptBtn.Normalizar();

        }
        //Animar Motor
        if (motorEncendido)
        {
            agitador.transform.Rotate(0, rpm * factorVelocidadAgitador * Time.deltaTime, 0);

            //Verifica que tenga algo de RPM para iniciar proceso de cambio de color
            if (rpm > 0)
            {

                //Cambiar a color verde obscuro mientras no este totalmente agitado y cuando ya se haya depositado la levadura necesaria
                if (!solucionTotalmenteAgitada && levadura.valor > valorMinimoAptoLevadura)
                {

                    //Guarda solo una vez el color actual, desde donde iniciara el lerp
                    if (!colorLerpAgitadoGuardado)
                    {
                        colorSolucionInicialAgitador = tanque.materialTanque.color;
                        colorLerpAgitadoGuardado = true;
                    }

                    tanque.materialTanque.color = Color.Lerp(colorSolucionInicialAgitador, colorTotalmenteAgitado, tiempoActualColorAgitado);
                    if (tiempoActualColorAgitado < 1)
                        tiempoActualColorAgitado += Time.deltaTime / duracionCambioColorAgitado;

                    //Verifica que ya tiene el color de totalmente agitado para no adicionar mas color
                    if (tanque.materialTanque.color == colorTotalmenteAgitado)
                        solucionTotalmenteAgitada = true;
                }
            }
        }

        // si es el primer dato 
        if (datosGrafico.Count == 0)
        {
            ReiniciarGrafica();
        }

        if (!Mathf.Approximately(datosGrafico[datosGrafico.Count - 1].temperatura, temperatura) ||
            !Mathf.Approximately(datosGrafico[datosGrafico.Count - 1].porcentajeAlcohol, _porcentajeAlcoholGrafica))
        {
            // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
            datosGrafico.Add(new DatosGrafico() { tiempo = tiempoActual, temperatura = temperatura, porcentajeAlcohol = _porcentajeAlcoholGrafica });
            datosGrafico.Add(new DatosGrafico() { tiempo = tiempoActual, temperatura = temperatura, porcentajeAlcohol = _porcentajeAlcoholGrafica });
        }
        else
        {
            DatosGrafico dato = datosGrafico[datosGrafico.Count - 1];
            dato.tiempo = tiempoActual;
            datosGrafico[datosGrafico.Count - 1] = dato;
        }
    }

    //Estado SimulacionTerminada
    private void EstadoSimulacionTerminada()
    {

    }

    //ContarTiempo
    private void ContarTiempo()
    {
        tiempoActual += Time.deltaTime;
        TimeSpan t = TimeSpan.FromSeconds(tiempoActual);
        //mostrar en el hud
        tiempoSimulacion = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Minutes, t.Seconds, t.Milliseconds / 10);
        simulador.pantalla.indicador_Tiempo_Usuario.text = tiempoSimulacion;

        //Primero verfica que haya pasado el tiempo normal, luego actualiza datos de adiccion de tiempo, y vuelve a comprobar el tiempo final
        if (tiempoActual >= tiempo_Usuario)
        {
            Simulador.ejecutandoEjercicio = false;
            Calcular_Variacion_Tiempo_X_Agitador();
            Calcula_Variacion_Tiempo_X_Azucar();
            if (tiempoActual >= tiempo_Usuario)
            {
                simulador.pantalla.ejecutandoEjercicio = false;
                CambiarEstado(EstadoSimulacionTerminada);
                CalcularAlcohol();
            }
        }
    }

    //CalcularResultado
    private void CalcularResultado()
    {
        Calcular_Variacion_X_Levadura();
        Calcular_Fermentacion_X_Temperatura();
        Calcular_Fermentacion_X_PH();

        if (tipo_Fermentacion_X_PH == Tipo_Fermentacion_X_PH.FERMENTACION_OPTIMA &&
        tipo_Fermentacion_X_Temperatura == Tipo_Fermentacion_X_Temperatura.FERMENTACION_OPTIMA)
        {
            nivel_Cultivo = Nivel_Cultivo.ALTO;
            simulador.pantalla.indicadores_Nivel_Cultivo_Color.color = colorVerde;
            simulador.pantalla.indicadores_Nivel_Cultivo_Texto.text = "100 %";
        }
        else
        {
            if (tipo_Fermentacion_X_PH == Tipo_Fermentacion_X_PH.FERMENTACION_OPTIMA &&
               tipo_Fermentacion_X_Temperatura == Tipo_Fermentacion_X_Temperatura.FERMENTACION_BUENA)
            {
                nivel_Cultivo = Nivel_Cultivo.MEDIO;
                simulador.pantalla.indicadores_Nivel_Cultivo_Color.color = colorAmarillo;
                simulador.pantalla.indicadores_Nivel_Cultivo_Texto.text = "50 %";
            }
            else
            {
                nivel_Cultivo = Nivel_Cultivo.BAJO;
                simulador.pantalla.indicadores_Nivel_Cultivo_Color.color = colorRojo;
                simulador.pantalla.indicadores_Nivel_Cultivo_Texto.text = "20 %";
            }
        }
    }

    //Calcula la presion cada 5 segundos segun los parametros del usuario
    private void CalcularPresion()
    {
        porcentaje_Alcohol_Final = 45;
        CalcularResultado();
        if (porcentaje_Alcohol_Final < 0)
            porcentaje_Alcohol_Final = 0;
        alcoholEtilico = (azucar.valor * porcentaje_Alcohol_Final) / 100;
        presion = alcoholEtilico;
        simulador.pantalla.indicadores_Presion.text = presion.ToString("f2");

        // Suavizar un poco 
        lerpAlcohol = Mathf.Lerp(lerpAlcohol, porcentaje_Alcohol_Final, Time.deltaTime / 15);
        simulador.pantalla.indicadores_Porcentaje_Alcohol.text = lerpAlcohol.ToString("f2");
        _porcentajeAlcoholGrafica = porcentaje_Alcohol_Final;
    }

    private void CalcularAlcohol()
    {
        porcentaje_Alcohol_Final = 45;
        CalcularResultado();
        if (porcentaje_Alcohol_Final < 0)
            porcentaje_Alcohol_Final = 0;
        alcoholEtilico = (azucar.valor * porcentaje_Alcohol_Final) / 100;
        presion = alcoholEtilico;
        simulador.pantalla.indicadores_Presion.text = presion.ToString("f2");
        CalcularEjercicio();
    }

    //Verifica que este bien ejecutado el ejercicio y en el tiempo estipulado
    private void CalcularEjercicio()
    {
        simulador.pantalla.panelEntradaDatos.gameObject.SetActive(false);
        // Borrar esto en la version final 

#if MODO_RAPIDO
		tiempo_Adicional = 0;
		porcentaje_Alcohol_Final = 45 ;	
#endif

        if (tiempo_Adicional == 0)
        {
            if (porcentaje_Alcohol_Final == 45)
            {
                simulador.pantalla.MostrarResultadoEjercicio(true);
            }
            else
            {
                simulador.pantalla.MostrarResultadoEjercicio(false);
            }
        }
        else
            simulador.pantalla.MostrarResultadoEjercicio(false);
    }

    //Calcula el % deducible de alcohol segun la levadura
    private void Calcular_Variacion_X_Levadura()
    {
        if (practicaLibre)
        {

        }

        if (levadura.valor < valorMinimoAptoLevadura)
        {
            porcentaje_Alcohol_Final -= porcentaje_Poca_Levadura;
            return;
        }

        if (levadura.valor > valorMinimoAptoLevadura && levadura.valor < valorMaximoAptoLevadura)
        {
            return;
        }

        if (levadura.valor > valorMaximoAptoLevadura)
        {
            porcentaje_Alcohol_Final -= porcentaje_Demasiada_Levadura;
            return;
        }
    }

    //Calcula el tiempo de mas del proceso segun las rpm
    private void Calcular_Variacion_Tiempo_X_Agitador()
    {
        if (rpm < valorMinimoAptoAgitador || rpm > valorMaximoAptoAgitador)
        {
            if (!purgado_Tiempo_X_Agitador)
            {
                tiempo_Adicional = tiempo_Adicional_X_RPM;
                tiempo_Usuario += tiempo_Adicional;
                simulador.pantalla.MostrarMensajeFermentacion(msjTiempoExtraRPM, 1);
                print("falla de agitador: " + tiempo_Adicional + " segundos");
                print("tiempo para terminar: " + tiempo_Usuario);
                purgado_Tiempo_X_Agitador = true;
            }
            else
                print("ya purgo agitador");
        }
    }

    //Calcula el tiempo de mas del proceso segun el azucar
    private void Calcula_Variacion_Tiempo_X_Azucar()
    {
        if (azucar.valor > 10)
        {
            if (!purgado_Tiempo_X_Azucar)
            {
                tiempo_Adicional = tiempo_Adicional_X_AZUCAR;
                tiempo_Usuario += tiempo_Adicional;
                simulador.pantalla.MostrarMensajeFermentacion(msjTiempoExtraAzucar);
                print("exceso de azucar: " + tiempo_Adicional + " segundos");
                print("tiempo para terminar: " + tiempo_Usuario);
                purgado_Tiempo_X_Azucar = true;
            }
            else
                print("ya purgo azucar");
        }
    }

    //Calcula el tipo de fermentacion resultante por temperatura
    private void Calcular_Fermentacion_X_Temperatura()
    {
        if (temperatura < valorMinimoTemperaturaBuena)
        {
            tipo_Fermentacion_X_Temperatura = Tipo_Fermentacion_X_Temperatura.FERMENTACION_NINGUNA;
            porcentaje_Alcohol_Final -= porcentaje_Temperatura_Ninguna;
        }
        if (temperatura == valorTemperaturaOptima)
        {
            tipo_Fermentacion_X_Temperatura = Tipo_Fermentacion_X_Temperatura.FERMENTACION_OPTIMA;
        }
        else
        {
            if (temperatura >= valorMinimoTemperaturaBuena && temperatura < valorMaximoTemperaturaBuena)
            {
                tipo_Fermentacion_X_Temperatura = Tipo_Fermentacion_X_Temperatura.FERMENTACION_BUENA;
                porcentaje_Alcohol_Final -= porcentaje_Temperatura_Buena;
            }
        }

        if (temperatura > valorMaximoTemperaturaBuena)
        {
            tipo_Fermentacion_X_Temperatura = Tipo_Fermentacion_X_Temperatura.MUERTE_BACTERIAS;
            porcentaje_Alcohol_Final -= porcentaje_Temperatura_Muerte;
        }
    }

    //Calcula el tipo de fermentacion resultante por PH
    private void Calcular_Fermentacion_X_PH()
    {
        if (PH < valorMinimoPH)
        {
            tipo_Fermentacion_X_PH = Tipo_Fermentacion_X_PH.MUERTE_BACTERIAS;
            porcentaje_Alcohol_Final -= porcentaje_PH_Muerte;
        }
        if (PH > valorMinimoPH && PH < valorMaximoPH)
        {
            tipo_Fermentacion_X_PH = Tipo_Fermentacion_X_PH.FERMENTACION_OPTIMA;
        }
        if (PH > valorMaximoPH)
        {
            tipo_Fermentacion_X_PH = Tipo_Fermentacion_X_PH.FERMENTACION_NINGUNA;
            porcentaje_Alcohol_Final -= porcentaje_PH_Ninguna;
        }
    }

    //Fijar PH
    private void FijarPH(float valor)
    {
        PH += valor * Time.deltaTime;

        if (PH > 14)
            simulador.pantalla.indicadores_PH.text = "14.00";
        if (PH < 0)
            simulador.pantalla.indicadores_PH.text = "0.00";
        if (PH > 0 && PH < 14)
            simulador.pantalla.indicadores_PH.text = PH.ToString("f2");
    }

    //Condiciona la animacion para que solo se relice de a una por vez
    private void TerminoAnimacionTemperatura()
    {
        animandoTemperatura = false;
        scriptBtnTempAgregar.Click(false);
        scriptBtnTempDisminuir.Click(false);
    }

    // se llama desde el hud cuando presiona iniciar
    public override void BtnIniciar(bool _practicaLibre)
    {
        usuarioEntroEstacion = true;

        practicaLibre = _practicaLibre;
        Simulador.ejecutandoEjercicio = true;
        //practicaLibre = _practicaLibre;
        CambiarEstado(EstadoIniciando);
        InicializarValores();
        //StartCoroutine(CalcularPresion());
        Simulador.ejecutandoEjercicio = true;
        if (!_practicaLibre)
        {
            ReiniciarGrafica();
        }

    }

    private void ClickBtnAzucar()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        azucar.valvulaAbierta = !azucar.valvulaAbierta;
        azucar.scriptBtn.Click(azucar.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnLevadura()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        levadura.valvulaAbierta = !levadura.valvulaAbierta;
        levadura.scriptBtn.Click(levadura.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnAcido()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        acido.valvulaAbierta = !acido.valvulaAbierta;
        acido.scriptBtn.Click(acido.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnBase()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        _base.valvulaAbierta = !_base.valvulaAbierta;
        _base.scriptBtn.Click(_base.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnMixerMas()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        if (motorEncendido)
        {
            if (rpm < rpm_Maxima)
            {
                rpm += 10;
                simulador.pantalla.rpmTexto.text = rpm.ToString("f2");
                scriptBtnRPMAgregar.StopCoroutine("ActivarBtInstantaneamente");
                scriptBtnRPMAgregar.StartCoroutine("ActivarBtInstantaneamente");
            }
        }
        else
        {
            //No activarlo si ya esta, ya que despues se llama una corrutina que lo desactiva
            if (simulador.pantalla.textoMensajeFermentacion[0].GetComponentInChildren<Text>().text != msjEnciendaAgitador)
                simulador.pantalla.MostrarMensajeFermentacion(msjEnciendaAgitador);
        }
    }

    private void ClickBtnMixerMenos()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();

        if (motorEncendido)
        {
            if (rpm > rpm_Minima)
            {
                rpm -= 10;
                simulador.pantalla.rpmTexto.text = rpm.ToString("f2");
                scriptBtnRPMDisminuir.StopCoroutine("ActivarBtInstantaneamente");
                scriptBtnRPMDisminuir.StartCoroutine("ActivarBtInstantaneamente");
            }
        }
        else
        {
            //No activarlo si ya esta, ya que despues se llama una corrutina que lo desactiva
            if (simulador.pantalla.textoMensajeFermentacion[0].GetComponentInChildren<Text>().text != msjEnciendaAgitador)
                simulador.pantalla.MostrarMensajeFermentacion(msjEnciendaAgitador);
        }
    }

    private void ClickBtnTempMas()
    {
        if (!animandoTemperatura)
        {
            if (_estado == EstadoIniciando)
                IniciarSimulacion();
            if (temperatura < temperatura_Maxima)
            {
                animandoTemperatura = true;
                temperatura += 1;
                simulador.pantalla.temperaturaTexto.text = temperatura.ToString("f2");
                _colorActual = new Color(1, materialResistencia.color.g - factorAdicionarTemperatura, materialResistencia.color.g - factorAdicionarTemperatura, 1);
                scriptBtnTempAgregar.Click();

                iTween.ColorTo(
                    resistencia.gameObject,
                    iTween.Hash(
                    "color", _colorActual,
                    "time", 0.5f,
                    "onComplete", "TerminoAnimacionTemperatura",
                    "onCompleteTarget", gameObject
                    )
                );
            }
        }
    }

    private void ClickBtnTempMenos()
    {
        if (!animandoTemperatura)
        {
            if (_estado == EstadoIniciando)
                IniciarSimulacion();
            if (temperatura > temperatura_Minima)
            {
                animandoTemperatura = true;
                temperatura -= 1;
                simulador.pantalla.temperaturaTexto.text = temperatura.ToString("f2");
                _colorActual = new Color(1, materialResistencia.color.g + factorAdicionarTemperatura, materialResistencia.color.g + factorAdicionarTemperatura, 1);
                scriptBtnTempDisminuir.Click();

                iTween.ColorTo(
                    resistencia.gameObject,
                    iTween.Hash(
                    "color", _colorActual,
                    "time", 0.5f,
                    "onComplete", "TerminoAnimacionTemperatura",
                    "onCompleteTarget", gameObject
                    )
                );
            }
        }
    }

    private void ClickBtnVaciarTanque()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        vaciarTanque = !vaciarTanque;
        tanqueDisponible = !tanqueDisponible;
        scriptBtnValvulaAlivio.Click(vaciarTanque);

        //Verifica si al vaciar queda poca levadura par Disponer cambio de color verde
        if (levadura.valor < 2.5f)
        {
            tiempoActualColorAgitado = 0;
            colorLerpAgitadoGuardado = false;
            solucionTotalmenteAgitada = false;
            tiempoActualColorInicial = 0;
            colorLerpInicialGuardada = false;
        }
    }

    private void ClickBtnEncendidoMotor()
    {
        if (_estado == EstadoIniciando)
            IniciarSimulacion();
        motorEncendido = !motorEncendido;
        scriptBtnAgitador.Click(motorEncendido);
    }

    #region Region_EasyTouch


    private void On_TouchUpReset(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject && usuarioEntroEstacion)
        {
            if (argBotonseleccionado == btnReset)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada && Simulador.activeBtnReset)
                {
                    print("Simulador.activeBtnReset: " + Simulador.activeBtnReset);
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.BtnReintentar();
                    ReiniciarGrafica();
                }
            }
        }
    }

    private void On_TouchUpPantalla(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject && usuarioEntroEstacion)
        {
            if (argBotonseleccionado == btnPantalla.gameObject)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
                {
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.AccionarPantalla();
                    simulador.pantalla.ActualizarDatosPantalla();
                }
            }
        }
    }

    public override void MostrarPantalla()
    {
        if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
        {
            simulador.pantalla.AccionarPantalla();
            simulador.pantalla.ActualizarDatosPantalla();
            return;
        }
        else
            print("cierre la pantalla o inicie el simulador");
    }

    // Detectar cuando toca un boton 3D 
    private void On_TouchUpBtnFermentacion(GameObject argBotonseleccionado)
    {
        if (argBotonseleccionado == null)
            return;

        if (simulador.simuladorActivo.gameObject == gameObject && dictEventosTouch.ContainsKey(argBotonseleccionado))
        {
            dictEventosTouch[argBotonseleccionado]();
        }
    }

    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void OnDestroy()
    {
        UnsubscribeEvent();
    }

    private void UnsubscribeEvent()
    {
        refSeleccionMouse._DltSeleccion -= On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion -= On_TouchUpReset;
        refSeleccionMouse._DltSeleccion -= On_TouchUpBtnFermentacion;

    }
    #endregion

    //Actualiza los datos del reporte
    public override void ActualizarDatosReporte()
    {
        CambiarEstado(EstadoSinActividad);
        tablaDatos.Clear();

        tablaDatos.Add(azucar.valor.ToString("f2") + " L");
        tablaDatos.Add(levadura.valor.ToString("f2") + " L");
        tablaDatos.Add(acido.valor.ToString("f2") + " L");
        tablaDatos.Add(_base.valor.ToString("f2") + " L");
        tablaDatos.Add(temperatura.ToString("f2"));
        tablaDatos.Add(rpm.ToString("f2") + " rpm");
        tablaDatos.Add(alcoholEtilico.ToString("f2") + " L");

        tablaDatos.Add(lerpAlcohol.ToString("f2") + " %");

        tablaDatos.Add(PH.ToString("f2"));
        tablaDatos.Add(presion.ToString("f2") + " psi");
        tablaDatos.Add(controlIdiomas.Traductor(nivel_Cultivo.ToString()) + " %");

        simulador.pantalla.reporte_Fecha.text = DateTime.Now.ToString("dd-MM-yyyy");
        simulador.simuladorActivo.reporte.GuardarTablaDatos(tablaDatosTextos, tablaDatos);
        simulador.pantalla.textoGrafica.text = nombreGrafica;
        simulador.simuladorActivo.reporte.GuardarNombrePDF(nombrePDF);
    }

    //Cuando se sale del simulador de fermentacion se llama esta funcion publica
    public void SalidaFermentacion()
    {
        CambiarEstado(EstadoSinActividad);
        InicializarValores();
    }

    //Cambia a estado simulacion
    public void IniciarSimulacion()
    {
        Simulador.activeBtnReset = true;
        simulador.pantalla.ejecutandoEjercicio = true;
        CambiarEstado(EstadoSimulacion);
        ejecutoVariosEjercicios = true;
    }

    //Obtener datos para graficar 	//TODO Especificar los que se necesiten
    public override float[] ObtenerDatosGrafica()
    {
        float[] _datos = new float[datosGrafico.Count * 3];
        int i = 0;
        int numDatos = datosGrafico.Count;
        foreach (var datoGrafico in datosGrafico)
        {
            _datos[i] = datoGrafico.tiempo;
            _datos[numDatos + i] = datoGrafico.temperatura;
            _datos[numDatos * 2 + i] = datoGrafico.porcentajeAlcohol;
            i++;
        }
        return _datos;
    }

    //Preguntas desde archivo externo
    public void RecibirPreguntasSimulador(string preguntas)
    {
        externalQuestions = preguntas.Split('&');
        refExternalQuestions.Add(externalQuestions[0]);
        refExternalQuestions.Add(externalQuestions[1]);
        refExternalQuestions.Add(externalQuestions[2]);
        refExternalQuestions.Add(externalQuestions[3]);
        refExternalQuestions.Add(externalQuestions[4]);
    }
}

//Clase Ingredientes
[Serializable]
public class Ingrediente
{
    public SimuladorFermentacion SimuladorFermentacion;
    public string nombre;
    public float valor;
    public bool valvulaAbierta;
    public enum Tipo { AZUCAR, LEVADURA, BASE, ACIDO };
    public Tipo tipo;
    public enum _Color { Blanco, Transparente, Verde };
    public _Color color;
    public AnimacionBotones scriptBtn;
}