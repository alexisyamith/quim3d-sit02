﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using TMPro;

#if UNITY_EDITOR
using System.IO;
#endif 

/// <summary>
/// Clase que controla el cambio de idioma de todos los textos del simulador
/// </summary>
public class ControlIdiomas : MonoBehaviour
{
    /// <summary>
    /// Enum con los idiomas diponibles
    /// </summary>
    public enum Idioma { Ingles, Espanol, Chino, Portugues, Turco };
    /// <summary>
    /// idioma actualmente seleccioando
    /// </summary>
    public Idioma idioma = Idioma.Espanol;
    /// <summary>
    /// Array con todos los Text del simulador
    /// </summary>
    public Text[] todosLabels;
    /// <summary>
    /// Array con todos los Text Mesh PRO GUI del simulador
    /// </summary>
    public TextMeshProUGUI[] todosTextMeshProGUI;
    /// <summary>
    /// Array con todos los TextMesh del simulador
    /// </summary>
    public TextMesh[] todosTextMesh;
    /// <summary>
    /// Referencia al game object que contiene en su jerarquia todo lo que se muestra en pantalla
    /// </summary>
    public GameObject Menu;
    /// <summary>
    /// Lista con todas las texturas en diferentes idiomas
    /// </summary>
    public List<CambioTextura> listaTexturas;
    /// <summary>
    /// Lista con todos los labes que contienen los check box de las respuestas a las preguntas
    /// </summary>
    public List<Text> labelCheckboxVerdadero;
    public List<Text> labelCheckboxFalso;
    /// <summary>
    /// Son los inputs File Plus del menu de inicio para 
    /// </summary>
    public InputField[] inputFilePlus;



    #if UNITY_EDITOR
        [ContextMenu("CogerTodosLabelsMenu")]
        void CogerTodosLabelsMenu()
        {
            todosLabels = Menu.transform.GetComponentsInChildren<Text>(true);
            print("CogerTodosLabelsMenu Cantidad:" + todosLabels.Length);
            string path = Application.dataPath + "/labelsQuimica.txt";

            if (File.Exists(path))
            {
                File.Delete(path);
                File.Create(path).Close();
            }
            else
            {
                File.Create(path).Close();
            }

            #if UNITY_ANDROID
                foreach (Text label in todosLabels)
                {
                    print(label.text);
                    File.AppendAllText(path, label.text + System.Environment.NewLine);
                }
            #endif
        }
#endif

#if UNITY_EDITOR
    [ContextMenu("CogerTodosTextMeshPro")]
    void CogerTodosTextMeshPro()
    {
        todosTextMeshProGUI = Menu.transform.GetComponentsInChildren<TextMeshProUGUI>(true);
        print("CogerTodosTextMeshPro Cantidad:" + todosLabels.Length);       
    }
#endif

    void Awake()
    {
        if (FindObjectOfType<Pantalla>().SelLangOption == false)
        {
            switch (idioma)
            {
                case Idioma.Ingles:

                    Extras.Diccionario.DictEspaIngles();                    

                    foreach (CambioTextura t in listaTexturas)
                    {
                        t.CambiarTexturaIngles();
                    }

                    foreach (Text l in labelCheckboxVerdadero)
                    {
                        l.text = "T";
                    }


                    foreach (Text l in labelCheckboxFalso)
                    {
                        l.text = "F";
                    }
                    break;

                case Idioma.Espanol:

                    foreach (CambioTextura t in listaTexturas)
                    {
                        t.CambiarTexturaEspanol();
                    }

                    foreach (Text l in labelCheckboxVerdadero)
                    {
                        l.text = "V";
                    }

                    foreach (Text l in labelCheckboxFalso)
                    {
                        l.text = "F";
                    }
                    break;

                case Idioma.Portugues:

                    Extras.Diccionario.DictEspa_Por();

                    foreach (CambioTextura t in listaTexturas)
                    {
                        t.CambiarTexturaPortuguese();
                    }

                    foreach (Text l in labelCheckboxVerdadero)
                    {
                        l.text = "V";
                    }

                    foreach (Text l in labelCheckboxFalso)
                    {
                        l.text = "F";
                    }

                    break;

                case Idioma.Turco:

                    Extras.Diccionario.DictEspaTurco();                    

                    foreach (CambioTextura t in listaTexturas)
                    {
                        t.CambiarTexturaTurco();
                    }

                    foreach (Text l in labelCheckboxVerdadero)
                    {
                        l.text = "D";
                    }

                    foreach (Text l in labelCheckboxFalso)
                    {
                        l.text = "S";
                    }

                    break;
            }

            TraducirTodosLabels();
            MtdtradusirInputs();
        }
    }

    /// <summary>
    /// Cambia el idioma a de todos los labels al idioma seleccionado
    /// </summary>
    public void TraducirTodosLabels()
    {
        foreach (var l in todosLabels)        
            Traductor(l);        

        foreach (var l in todosTextMeshProGUI)        
            Traductor(l);        

        // TraductorTextMesh
        foreach (var l in todosTextMesh)        
            TraductorTextMesh(l);        
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// traduce los inputs text
    /// </summary>
    public void MtdtradusirInputs() {

		if (FindObjectOfType<Simulador> ().tipoAplicacion == Simulador.TipoAplicacion.WEB && FindObjectOfType<Simulador> ().ModoLTI == true) {
			inputFilePlus[0].text = FindObjectOfType<Simulador> ().usuario;
			inputFilePlus[1].text = FindObjectOfType<Simulador> ().id_curso;
			inputFilePlus[2].text = FindObjectOfType<Simulador> ().curso;
			inputFilePlus[3].text = FindObjectOfType<Simulador> ().institucion;	
			inputFilePlus [0].readOnly = true;
			inputFilePlus [1].readOnly = true;
			inputFilePlus [2].readOnly = true;
			inputFilePlus [3].readOnly = true;

		} else {

			if (FindObjectOfType<Simulador> ().ModoMonoUsuario == true) {
				inputFilePlus[0].text = Traductor(FindObjectOfType<Simulador> ().mono_user_name);
				inputFilePlus[1].text = Traductor(inputFilePlus[1].text);
				inputFilePlus[2].text = Traductor(inputFilePlus[2].text);
				inputFilePlus[3].text = Traductor(FindObjectOfType<Simulador> ().mono_user_institution);
				inputFilePlus[0].readOnly = true;
				inputFilePlus[1].readOnly = false;
				inputFilePlus[2].readOnly = false;
				inputFilePlus[3].readOnly = true;
			} else {
				inputFilePlus[0].text = Traductor(inputFilePlus[0].text);
				inputFilePlus[1].text = Traductor(inputFilePlus[1].text);
				inputFilePlus[2].text = Traductor(inputFilePlus[2].text);
				inputFilePlus[3].text = Traductor(inputFilePlus[3].text);
				inputFilePlus[0].readOnly = false;
				inputFilePlus[1].readOnly = false;
				inputFilePlus[2].readOnly = false;
				inputFilePlus[3].readOnly = false;
			}
		}

        

    }



    /// <summary>
    /// Traduce el contenido del Text al idioma seleccionado
    /// </summary>
    /// <param name="label">Label q</param>
    public void Traductor(Text label)
    {
        var tmpTextLabel = label.text.ToLower();

        switch (idioma)
        {
            case Idioma.Ingles:

                if (Extras.Diccionario.espanol_Ingles.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Ingles[tmpTextLabel];
                    Debug.Log("Traductor Ingles Text.label : " + tmpTextLabel + " = " + label.text);
                }
                /*else
                {
                    var tmpParent = label.transform;
                    var tmpNombrePadres = tmpParent.name+"/";

                    while (tmpParent != null)
                    {
                        tmpParent = tmpParent.parent;
                        if (tmpParent != null)
                            tmpNombrePadres += tmpParent.name + "/";
                    }

                    Debug.LogError("Traductor Ingles Text.label NO ENCONTRADA : "+ tmpTextLabel + " en "+ tmpNombrePadres);
                }*/

                break;

            case Idioma.Portugues:

                if (Extras.Diccionario.espanol_Portugues.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Portugues[tmpTextLabel];
                    Debug.Log("Traductor Portugues Text.label : " + tmpTextLabel + " = "+ label.text);
                }
               /* else
                {//para localizar facilmente los labels que estan mal signados
                    var tmpParent = label.transform;
                    var tmpNombrePadres = tmpParent.name + "/";

                    while (tmpParent != null)
                    {
                        tmpParent = tmpParent.parent;
                        if (tmpParent != null)
                            tmpNombrePadres += tmpParent.name + "/";
                    }

                    Debug.LogError("Traductor Portugues Text.label NO ENCONTRADA : " + tmpTextLabel + " en " + tmpNombrePadres);
                }*/

                break;

            case Idioma.Turco:

                if (Extras.Diccionario.espanol_Turco.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Turco[tmpTextLabel];
                    Debug.Log("Traductor Turco Text.label : " + tmpTextLabel + " = " + label.text);
                }
                /*else
                {
                    var tmpParent = label.transform;
                    var tmpNombrePadres = tmpParent.name+"/";

                    while (tmpParent != null)
                    {
                        tmpParent = tmpParent.parent;
                        if (tmpParent != null)
                            tmpNombrePadres += tmpParent.name + "/";
                    }

                    Debug.LogError("Traductor Ingles Text.label NO ENCONTRADA : "+ tmpTextLabel + " en "+ tmpNombrePadres);
                }*/

                break;
        }
    }

    /// <summary>
    /// Traduce el contenido del Text al idioma seleccionado
    /// </summary>
    /// <param name="label">Label q</param>
    public void Traductor(TextMeshProUGUI label)
    {
        var tmpTextLabel = label.text.ToLower();

        switch (idioma)
        {
            case Idioma.Ingles:

                if (Extras.Diccionario.espanol_Ingles.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Ingles[tmpTextLabel];
                    Debug.Log("Traductor Ingles Text.label : " + tmpTextLabel + " = " + label.text);
                }
                break;

            case Idioma.Portugues:

                if (Extras.Diccionario.espanol_Portugues.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Portugues[tmpTextLabel];
                    Debug.Log("Traductor Portugues Text.label : " + tmpTextLabel + " = " + label.text);
                }
                break;

            case Idioma.Turco:

                if (Extras.Diccionario.espanol_Turco.ContainsKey(tmpTextLabel))
                {
                    label.text = Extras.Diccionario.espanol_Turco[tmpTextLabel];
                    Debug.Log("Traductor Turco Text.label : " + tmpTextLabel + " = " + label.text);
                }
                break;
        }
    }


    /// <summary>
    /// Traduce un texto al idioma seleccionado
    /// </summary>
    /// <param name="label">Texto que se desea traducir</param>
    /// <returns>Texto traducido</returns>
    public string Traductor(string label)
    {
        var tmpTextLabel = label.ToLower();

        switch (idioma)
        {
            case Idioma.Ingles:

                if (Extras.Diccionario.espanol_Ingles.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Ingles[tmpTextLabel];
                    Debug.Log("Traductor Ingles label : "+ label + " = "+ tmpTextTraducido);
                    return tmpTextTraducido;
                }
                else
                    Debug.Log("Traductor Ingles label NO ENCONTRADA : " + label);

                return label;

            case Idioma.Portugues:

                if (Extras.Diccionario.espanol_Portugues.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Portugues[tmpTextLabel];
                    Debug.Log("Traductor Portugues label : " + label + " = "+ tmpTextTraducido);
                    return tmpTextTraducido;
                }
                else
                    Debug.Log("Traductor Portugues label NO ENCONTRADA : " + label);

                return label;

            case Idioma.Turco:

                if (Extras.Diccionario.espanol_Turco.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Turco[tmpTextLabel];
                    Debug.Log("Traductor Turco label : " + label + " = " + tmpTextTraducido);
                    return tmpTextTraducido;
                }
                else
                    Debug.Log("Traductor Turco label NO ENCONTRADA : " + label);

                return label;

            default:
                return label;
        }
    }

    /// <summary>
    /// Traduce los textos de los text mesh
    /// </summary>
    /// <param name="label"></param>
    public void TraductorTextMesh(TextMesh label)
    {
        var tmpTextLabel = label.text.ToLower();

        switch (idioma)
        {
            case Idioma.Ingles:

                if (Extras.Diccionario.espanol_Ingles.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Ingles[tmpTextLabel];
                    Debug.Log("TraductorTextMesh Ingles label : " + label.text + " = " + tmpTextTraducido);
                    label.text = tmpTextTraducido;
                }
                else
                    Debug.Log("TraductorTextMesh Ingles label NO ENCONTRADA : " + label.text);

                break;

            case Idioma.Portugues:

                if (Extras.Diccionario.espanol_Portugues.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Portugues[tmpTextLabel];
                    Debug.Log("TraductorTextMesh Ingles label : " + label.text + " = " + tmpTextTraducido);
                    label.text = Extras.Diccionario.espanol_Portugues[tmpTextLabel];
                }
                else
                    Debug.Log("TraductorTextMesh Ingles label NO ENCONTRADA : " + label.text);

                break;

            case Idioma.Turco:

                if (Extras.Diccionario.espanol_Turco.ContainsKey(tmpTextLabel))
                {
                    var tmpTextTraducido = Extras.Diccionario.espanol_Turco[tmpTextLabel];
                    Debug.Log("TraductorTextMesh Turco label : " + label.text + " = " + tmpTextTraducido);
                    label.text = Extras.Diccionario.espanol_Turco[tmpTextLabel];
                }
                else
                    Debug.Log("TraductorTextMesh Ingles label NO ENCONTRADA : " + label.text);

                break;
        }
    }
    
    /// <summary>
    /// Carga de español a inglés
    /// </summary>
    public void cambio_spa_to_eng_labels()
    {
        Extras.Diccionario.DictEspaIngles();
        // guardar los textos originales 
        TraducirTodosLabels();

        switch (idioma)
        {
            case Idioma.Ingles:

                // cambiar al ingles
                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaIngles();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "T";
                }

                break;

            case Idioma.Espanol:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaEspanol();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Portugues:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaPortuguese();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Turco:

                // cambiar al ingles
                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaTurco();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "D";
                }

                break;
        }
    }

    /// <summary>
    /// Cargar los labels de español a portugues
    /// </summary>
    public void cambio_spa_to_portugues_labels()
    {
        Extras.Diccionario.DictEspa_Por();
        // guardar los textos originales 
        TraducirTodosLabels();

        switch (idioma)
        {
            case Idioma.Ingles:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaIngles();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "T";
                }

                break;

            case Idioma.Espanol:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaEspanol();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Portugues:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaPortuguese();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Turco:

                // cambiar al ingles
                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaTurco();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "D";
                }

                break;
        }
    }

    public void cambio_spa_to_turco_labels()
    {
        Extras.Diccionario.DictEspaTurco();
        // guardar los textos originales 
        TraducirTodosLabels();

        switch (idioma)
        {
            case Idioma.Ingles:

                // cambiar al ingles
                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaIngles();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "T";
                }

                break;

            case Idioma.Espanol:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaEspanol();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Portugues:

                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaPortuguese();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "V";
                }

                break;

            case Idioma.Turco:

                // cambiar al Turco
                foreach (CambioTextura t in listaTexturas)
                {
                    t.CambiarTexturaTurco();
                }

                foreach (Text l in labelCheckboxVerdadero)
                {
                    l.text = "D";
                }

                break;
        }
    }

    /// <summary>
    /// Cargar los labels de español a turco
    /// </summary>
}


[Serializable]
public class CambioTextura
{
    //public UITexture uitexture; hadron
    public Texture texturaEspanol;
    public Texture texturaIngles;
    public Texture texturaPortugues;
    public Texture texturaTurco;
    
    public void CambiarTexturaIngles()
    {
        //uitexture.mainTexture = texturaIngles; hadron
    }

    public void CambiarTexturaEspanol()
    {
        //uitexture.mainTexture = texturaEspanol; hadron
    }

    public void CambiarTexturaPortuguese()
    {
        //uitexture.mainTexture = texturaPortugues; hadron
    }

    public void CambiarTexturaTurco()
    {
        //uitexture.mainTexture = texturaTurco; hadron
    }
}