﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class Evaluacion : MonoBehaviour
{
    /// <summary>
    /// Para asignar los textos de evaluacion
    /// </summary>
    public Pantalla pantalla;
    /// <summary>
    /// Lista de preguntas del simulador
    /// </summary>
    public List<string> referenciaPreguntas = new List<string>();
    /// <summary>
    /// Lista con todas las respuestas del usuario
    /// </summary>
    public List<bool> respuestasUsuario = new List<bool>();
    /// <summary>
    /// Clase que controla las principales variables de la simulacion
    /// </summary>
    public Simulador simulador;
    /// <summary>
    /// Toggles para marcar las preguntas correctas y incorrectas
    /// </summary>
    public Toggle[] toggles;
    /// <summary>
    /// Toggles group en donde se agrupan las respuestas en pares para hacer posible solo la seleccion de un toggle verdadero o falso.
    /// </summary>
    [SerializeField]
    private ToggleGroup[] togglesGroup;

    /// <summary>
    /// Desordena todas las preguntas de un simulador seleccionado
    /// </summary>
    public void DesordenarPreguntas()
    {
        referenciaPreguntas.Clear();
        if (simulador.tipoAplicacion == Simulador.TipoAplicacion.WEB_LOCAL)
        {
            if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
                referenciaPreguntas = simulador.simuladorActivo.GetComponent<SimuladorFermentacion>().listaPreguntas;
            if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
                referenciaPreguntas = simulador.simuladorActivo.GetComponent<SimuladorDestilacion>().listaPreguntas;
            if(simulador.simuladorActivo.GetType() == typeof (SimuladorAminoAcido))
				referenciaPreguntas = simulador.simuladorActivo.GetComponent<SimuladorAminoAcido>().listaPreguntas;
        }
        else
        {
            for (int i = 0; i < simulador.simuladorActivo.listaPreguntas.Count; i++)
            {
                referenciaPreguntas.Add(simulador.simuladorActivo.listaPreguntas[i]);
            }
        }

        referenciaPreguntas = ShuffleList(referenciaPreguntas);

        for (int i = 0; i < referenciaPreguntas.Count; i++)
        {
            string[] _texts = referenciaPreguntas[i].Split(':');
            pantalla.textoEvaluacion[i].text = "• " + _texts[0];
        }

        simulador.simuladorActivo.reporte.GuardarPreguntas(referenciaPreguntas);
    }

    /// <summary>
    /// Desordena la lista
    /// </summary>
    /// <typeparam name="E">Tipo de la lista que se quiere desordenar</typeparam>
    /// <param name="list">Lista que se quiere desordenar</param>
    public List<E> ShuffleList<E>(List<E> list)
    {
        var randomList = new List<E>();
        var r = new Random();
        int randomIndex = 0;

        while (list.Count > 0)
        {
            randomIndex = r.Next(0, list.Count); //Choose a random object in the list
            randomList.Add(list[randomIndex]); //add it to the new, random list
            list.RemoveAt(randomIndex); //remove to avoid duplicates
        }

        return randomList; //return the new random list
    }

    /// <summary>
    /// Borra las respuesta
    /// </summary>
    [ContextMenu("Borrar Respuestas")]
    public void BorrarRespuestas()
    {
        foreach (ToggleGroup toggle in togglesGroup)
        {
            toggle.SetAllTogglesOff();
        }
    }

    /// <summary>
    /// Agrega posibles respuestas de usuario
    /// </summary>
    /// <returns></returns>
    public bool AgregarRespuestas()
    {
        for (int i = 0; i < toggles.Length/2; i++)
        {
            if (!toggles[i].isOn && !toggles[i+5].isOn)
            {
                print("No se han llenado todas las preguntas ");
                return false;
            }
        }

        respuestasUsuario.Clear();
        respuestasUsuario.Add(RespuestaEnPregunta(1));
        respuestasUsuario.Add(RespuestaEnPregunta(2));
        respuestasUsuario.Add(RespuestaEnPregunta(3));
        respuestasUsuario.Add(RespuestaEnPregunta(4));
        respuestasUsuario.Add(RespuestaEnPregunta(5));
        simulador.simuladorActivo.reporte.GuardarRespuestas(respuestasUsuario);
        return true;
    }

    /// <summary>
    /// verifica si la respuesta corresponde a la pregunta 
    /// </summary>
    /// <param name="grupo">grupo que se va a verificar</param>
    public bool RespuestaEnPregunta(int grupo)
    {
        foreach (Toggle toggle in toggles)
        {
            string nombrePregunta = "Verdadero" + grupo;

            if (toggle.name.Equals(nombrePregunta) && toggle.isOn && toggle.group ==  togglesGroup[grupo-1])
            {
                return true;
            }
        }
        
        return false;        
    }
}