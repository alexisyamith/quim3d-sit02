﻿using UnityEngine;

/// <summary>
/// Clase que controla las versiones
/// </summary>
public class ControlVersiones : MonoBehaviour
{
    public string version = "1.5";

    #if UNITY_EDITOR
        [ContextMenu("SetBundleVersion")]
        void SetBundleVersion()
        {

            UnityEditor.PlayerSettings.bundleVersion = version;

        }
    #endif

    void OnGUI()
    {
        Rect pos = new Rect(Screen.width - 100, Screen.height - 50, Screen.width, 100);
        GUI.Label(pos, "Version " + version);
    }
}
