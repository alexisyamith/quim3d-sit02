﻿using UnityEngine;

/// <summary>
/// Clase que combina los mesh de todos los hijos
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer)), ExecuteInEditMode]
public class CombineMeshC : MonoBehaviour
{
    private void Start()
    {
        
        foreach (Transform child in transform)
        {
            child.position += transform.position;
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;
        }

        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        var combine = new CombineInstance[meshFilters.Length];
        int i = 0;

        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
            i++;
        }

        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        GetComponent<Renderer>().material = meshFilters[1].GetComponent<Renderer>().sharedMaterial;
        transform.gameObject.SetActive(true);
    }
}