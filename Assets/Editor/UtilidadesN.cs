﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Timers;
using System.Threading;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class UtilidadesN
{
    //VARAIBLES

    //----------------------
    //---APP NAME
    const string COMPANY = "CloudLabs";

    //SPA
    static string PRODUCT_SPA = "CloudLabs Química 3D - Estudio de la destilacion de alcohol";
    static string PRODUCT_SPA_OSX = "CloudLabs - Quimica 3D";
    static string PRODUCT_ENG = "CloudLabs - DISTILLER";
    static string PRODUCT_ENG_OSX = "CloudLabs - Chemistry 3D";
    static string PRODUCT_POR = "CloudLabs - Estudo da destilação de álcool";
    static string PRODUCT_POR_OSX = "CloudLabs - Estudo da destilacao de alcool";
    static string PRODUCT_TUR = "CloudLabs - DISTILLER";
    static string PRODUCT_TUR_OSX = "CloudLabs - DISTILLER OSX";

    

    //----------------------
    //---identifier
    static string SPA_ID = "com.cloudlabs.separacion.estaciondestilacion.v4";
    static string ENG_ID = "com.cloudlabs.eng.separacion.estaciondestilacion.v4";
    static string POR_ID = "com.cloudlabs.por.separacion.estaciondestilacion.v4";
    static string TUR_ID = "com.cloudlabs.tur.separacion.estaciondestilacion.v4";


    //----------------------
    //---LANG
    /*static menu.LanguageList Lang_SPA = menu.LanguageList.Spanish;
    static menu.LanguageList Lang_ENG = menu.LanguageList.English;
    static menu.LanguageList Lang_POR = menu.LanguageList.Portuguese;*/


    //---BIL LANG
    /*static menu.ConfigureLanguageList SPA_ENG = menu.ConfigureLanguageList.Spanish_English;
    static menu.ConfigureLanguageList POR_ENG = menu.ConfigureLanguageList.English_Portuguese;*/

    //----
    static bool SECURITY;

    //keyword
    static string simKeyword = "clbs_quim3d02";
    static string std_folder = @"DEPLOY\Release\STD\";
    static string mono_folder = @"DEPLOY\Release\MONO\";

    //AULA
    static void AULASel_ON()
    {
        GameObject.FindObjectOfType<Simulador>().ModoAula = true;
    }
    static void AULASel_OFF()
    {
        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
    }

    //IDIOMAS
    public static void CambiarIngles()
    {
        /*PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Physics CloudLabs";//Nombre de la aplicación

        PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";
        //Debug.Log (PlayerSettings.bundleIdentifier);
#if UNITY_IOS
        if (GameObject.FindObjectOfType<Simulador>().SeguridadSwitch == false)
        {
            string IdiomaActual;
            ControlIdiomas Ctrl_Idi_Ref = GameObject.FindObjectOfType<ControlIdiomas>();
            IdiomaActual = (Ctrl_Idi_Ref.idioma).ToString();

            if (IdiomaActual == "Ingles")
            {
                Debug.Log("INGLES - IOS");
                PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
                PlayerSettings.productName = "Physics CloudLabs";//Nombre de la aplicación
                PlayerSettings.applicationIdentifier = "com.genmedia.fisicaeng";
            }
            else
            {
                Debug.Log("ESPANOL");
                PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
                PlayerSettings.productName = "CloudLabs Física";//Nombre de la aplicación
                PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";
            }
        }
#endif
   */     
        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Ingles;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            
            sim.CargarTextos();
        }
    }
    public static void CambiarEspanol()
    {
        /*PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Física CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";*/
        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Espanol;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            
            sim.CargarTextos();
        }
    }
    public static void CambiarPortugues()
    {
        /*PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Física CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";*/
        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Portugues;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            
            sim.CargarTextos();
        }
    }

    public static void CambiarTurco()
    {
        /*PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Physics CloudLabs";//Nombre de la aplicación

        PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";
        //Debug.Log (PlayerSettings.bundleIdentifier);
#if UNITY_IOS
        if (GameObject.FindObjectOfType<Simulador>().SeguridadSwitch == false)
        {
            string IdiomaActual;
            ControlIdiomas Ctrl_Idi_Ref = GameObject.FindObjectOfType<ControlIdiomas>();
            IdiomaActual = (Ctrl_Idi_Ref.idioma).ToString();

            if (IdiomaActual == "Ingles")
            {
                Debug.Log("INGLES - IOS");
                PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
                PlayerSettings.productName = "Physics CloudLabs";//Nombre de la aplicación
                PlayerSettings.applicationIdentifier = "com.genmedia.fisicaeng";
            }
            else
            {
                Debug.Log("ESPANOL");
                PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
                PlayerSettings.productName = "CloudLabs Física";//Nombre de la aplicación
                PlayerSettings.applicationIdentifier = "com.genmedia.fisicaesp";
            }
        }
#endif
   */     
        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Turco;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            
            sim.CargarTextos();
        }
    }

    //--BILINGUE
    static void LangSel_ON_EngSpa()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = true;
        GameObject.FindObjectOfType<Pantalla>().pairLanguages = "ENG-SPA";
    }
    static void LangSel_ON_EngPor()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = true;
        GameObject.FindObjectOfType<Pantalla>().pairLanguages = "ENG-POR";
    }
    static void LangSel_OFF()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = false;
    }

    //LOGO
    public static void UsarLogo1()
    {
        GameObject.FindObjectOfType<Simulador>().UsarLogo1();
    }

    //SEGURIDAD
    static void ActivarSeguridad()
    {
        //GameObject.FindObjectOfType<Simulador>().SeguridadSwitch
    }
    static void QuitarSeguridad()
    {
        GameObject.FindObjectOfType<Simulador>().QuitarSeguridad();
    }



    [MenuItem("IE/Seguridad")]
    public static void Seguridad()
    {

        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = !GameObject.FindObjectOfType<Simulador>().SeguridadSwitch;
        SECURITY = GameObject.FindObjectOfType<Simulador>().SeguridadSwitch;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        if (GameObject.FindObjectOfType<Simulador>().SeguridadSwitch)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }

    }


    [MenuItem("IE/Build")]
    static void Build()
    {
        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;

        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = true;
        SECURITY = GameObject.FindObjectOfType<Simulador>().SeguridadSwitch;


        // sim_SPA_webGL();
        //sim_ENG_webGL();
        //sim_POR_webGL();

        //sim_SPA_webGL_AULA();
        // sim_SPA_webGL_AULA_nuevo();
        //sim_ENG_webGL_AULA();
        //sim_ENG_webGL_AULA_nuevo();
        //sim_POR_webAula();*/
        sim_POR_webGL_AULA_nuevo();



        //sim_SPA_PC();
        //sim_SPA_PC_mono();
        //sim_ENG_PC();
        //sim_ENG_PC_mono();
        sim_POR_PC();
        //sim_POR_PC_mono();
        //sim_BIL_ESP_ENG();
        //sim_BIL_ESP_ENG_mono();
        //sim_BIL_POR_ENG();
        //sim_BIL_POR_ENG_mono();*//**/



        //sim_SPA_OSX();
        //sim_SPA_OSX_mono();
        //sim_ENG_OSX();
        //sim_ENG_OSX_mono();*/
        sim_POR_OSX();
        //sim_POR_OSX_mono();
        //sim_BIL_ESP_ENG_OSX();
        //sim_BIL_ESP_ENG_mono_OSX();
        //sim_BIL_POR_ENG_OSX();
        //sim_BIL_POR_ENG_mono_OSX();*/



        //sim_SPA_APK();
        //sim_SPA_APK_mono();
        //sim_ENG_APK();
        //sim_ENG_APK_mono();
        //sim_POR_APK();
        /*sim_POR_APK_mono();*/
        /*sim_BIL_ESP_ENG_APK();
        sim_BIL_ESP_ENG_mono_APK();
        sim_BIL_POR_ENG_APK();
        sim_BIL_POR_ENG_mono_APK();*/

        //sim_TUR_webGL();

    }


    /*[MenuItem("IE/test")]
    static void test()
    {
        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);
    }*/


    static BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
    static void groupScenes()
    {


        buildPlayerOptionsScene.scenes = new[] { "Assets/Escenas/SimuladorQuimica.unity" };
    }

    //Exportaciones
    //PC - SPA
    static void sim_SPA_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //GameObject.FindObjectOfType<Simulador>().e = Lang_SPA;
        //GameObject.FindObjectOfType<menu>().LanguageConf = SPA_ENG;
        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        /*BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        

        buildPlayerOptionsScene.scenes = new[] { "Assets/Scenes/MainMenu.unity",
            "Assets/Scenes/situacion_1.unity",
            "Assets/Scenes/situacion_2.unity",
            "Assets/Scenes/situacion_3.unity",
            "Assets/Scenes/situacion_libre.unity"};*/

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std/" + simKeyword + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_PC_mono()
    {
        
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        //BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, @"DEPLOY\MONO\autom_mesaplc_graf_spa_std\autom_mesaplc_graf_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);
        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono/" + simKeyword + "_spa_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_std/" + simKeyword + "_eng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_PC_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_mono/" + simKeyword + "_eng_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_POR.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_std/" + simKeyword + "_por_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_PC_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_POR.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_mono/" + simKeyword + "_por_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }

    static void sim_BIL_ESP_ENG()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_espeng_std/" + simKeyword + "_b_espeng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_ESP_ENG_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_espeng_mono/" + simKeyword + "_b_espeng_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_engpor_std/" + simKeyword + "_b_engpor_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BPORENG_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_engpor_mono/" + simKeyword + "_b_engpor_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BPORENG_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }

    //OSX - SPA
    static void sim_SPA_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;


        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_OSX_mono()
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;


        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_m_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;


        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_s_x/" + PRODUCT_ENG_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_OSX_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;


        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_m_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_POR.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;


        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_s_x/" + PRODUCT_POR_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_OSX_mono()
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;


        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_m_x/" + PRODUCT_POR + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }

    static void sim_BIL_ESP_ENG_OSX()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_espeng_s_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_ESP_ENG_mono_OSX()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_espeng_m_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);
        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG_OSX()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_engpor_s_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BPORENG_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG_mono_OSX()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_engpor_m_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BPORENG_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }


    //APK - SPA
    static void sim_SPA_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;


        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_APK_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, ENG_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_APK_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, ENG_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, POR_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_APK_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, POR_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }

    static void sim_BIL_ESP_ENG_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_espeng_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);




        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_ESP_ENG_mono_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngSpa();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_espeng_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_b_engpor_std.apk", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);



        //----------------------------------------------------------------------------------------
    }
    static void sim_BIL_POR_ENG_mono_APK()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        CambiarEspanol();
        LangSel_ON_EngPor();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = true;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_b_engpor_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);



        //----------------------------------------------------------------------------------------
    }


    // webGL")]
    static void sim_SPA_webGL()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = true;



        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    static void sim_ENG_webGL()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = true;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    static void sim_POR_webGL()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = true;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    static void sim_TUR_webGL()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarTurco();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = true;

        
        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_tur_lti/" + simKeyword + "_tur_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_lti/" + simKeyword + "_tur_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_TUR_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    // webGL")]
    static void sim_SPA_webGL_AULA()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;
        GameObject.FindObjectOfType<Simulador>().ModoAula = true;

         GameObject.FindObjectOfType<Simulador>().WebGLUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";



        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula/" + simKeyword + "_spa_webAula";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAula/" + simKeyword + "_spa_webAula", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAula.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());  

    }

    static void sim_SPA_webGL_AULA_nuevo()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarEspanol();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;
        GameObject.FindObjectOfType<Simulador>().ModoAula = true;
        
        GameObject.FindObjectOfType<Simulador>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula_nuevo/" + simKeyword + "_spa_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAula_nuevo/" + simKeyword + "_spa_webAula_nuevo", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAula_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }

    static void sim_POR_webGL_AULA_nuevo()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarPortugues();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;
        GameObject.FindObjectOfType<Simulador>().ModoAula = true;
        
        GameObject.FindObjectOfType<Simulador>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_por_webAula_nuevo/" + simKeyword + "_por_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_webAula_nuevo/" + simKeyword + "_por_webAula_nuevo", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_webAula_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }

    // webGL")]
    static void sim_ENG_webGL_AULA()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;

        GameObject.FindObjectOfType<Simulador>().ModoAula = true;

        GameObject.FindObjectOfType<Simulador>().WebGLUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";




        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula/" + simKeyword + "_spa_webAula";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAula/" + simKeyword + "_eng_webAula", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAula.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }

    static void sim_ENG_webGL_AULA_nuevo()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        CambiarIngles();
        LangSel_OFF();

        GameObject.FindObjectOfType<Simulador>().ModoMonoUsuario = false;
        GameObject.FindObjectOfType<Simulador>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<Simulador>().ModoLTI = false;

        GameObject.FindObjectOfType<Simulador>().ModoAula = true;

        GameObject.FindObjectOfType<Simulador>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";




        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula_nuevo/" + simKeyword + "_spa_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAula_nuevo/" + simKeyword + "_eng_webAula_nuevo", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAula_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }





}